-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-10-2017 a las 01:14:02
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `infirmary`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aquestion`
--

CREATE TABLE `aquestion` (
  `ID` int(15) NOT NULL,
  `Q1` varchar(100) DEFAULT NULL,
  `Q2` varchar(100) DEFAULT NULL,
  `Q3` int(2) DEFAULT NULL,
  `Q4` int(10) DEFAULT NULL,
  `Q5` varchar(100) DEFAULT NULL,
  `Q6` varchar(100) DEFAULT NULL,
  `Q7` varchar(100) DEFAULT NULL,
  `Q8` varchar(100) DEFAULT NULL,
  `Q9` varchar(100) NOT NULL,
  `Q10` varchar(100) NOT NULL,
  `Q11` varchar(100) NOT NULL,
  `Q12` varchar(100) NOT NULL,
  `TS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aquestion`
--

INSERT INTO `aquestion` (`ID`, `Q1`, `Q2`, `Q3`, `Q4`, `Q5`, `Q6`, `Q7`, `Q8`, `Q9`, `Q10`, `Q11`, `Q12`, `TS`) VALUES
(1, 'a', 'a', 1, 1, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', '2017-09-21 18:49:36'),
(2, 'a', 'a', 2, 12, 'sdd', 'ss', 'a', 'a', 'a', 'a1', 'dd', 'ss', '2017-09-21 18:53:40'),
(3, 'a', 'a', 2, 12, 'sdd', 'ss', 'a', 'a', 'a', 'a1', 'dd', 'ss', '2017-09-21 18:54:19'),
(4, 'a', 'a', 3, 1, 'a', 'a', 'a', 'a', 'aa', 'a', 'a', 'a', '2017-09-21 18:56:07'),
(5, 'a', 'a', 3, 1, 'a', 'a', 'a', 'a', 'aa', 'a', 'a', 'a', '2017-09-21 18:56:46'),
(6, 'a', 'a', 3, 1, 'a', 'a', 'a', 'a', 'aa', 'a', 'a', 'a', '2017-09-21 18:57:21'),
(7, 'a', 'a', 3, 1, 'a', 'a', 'a', 'a', 'aa', 'a', 'a', 'a', '2017-09-21 19:02:32'),
(8, 'a', 'a', 3, 1, 'a', 'a', 'a', 'a', 'aa', 'a', 'a', 'a', '2017-09-21 19:03:21'),
(9, 'null', 'null', NULL, NULL, 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', '2017-10-03 00:01:11'),
(10, 'null', 'null', NULL, NULL, 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', '2017-10-03 00:48:43'),
(11, 'njknkjnn', 'kjnjknkk', 5, 89987987, 'jnknkjn', 'jnjnknj', 'kjnjnkjn', 'jnknkj', 'kmlmklm', 'nkjnkjnk', 'njkjnkjn', 'jnkjn', '2017-10-03 00:53:35'),
(12, 'null', 'null', NULL, NULL, 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', '2017-10-03 00:58:51'),
(13, '|nkjnkjn|nkjn|', 'knk', 8, 798, 'nj', 'kjn', 'njnknn', 'nkj', 'nkj', 'n', 'uih', 'n', '2017-10-03 01:14:15'),
(14, 'afasdgfas', 'asdgfsaf', 2, 4321412, 'hdhfgas', 'gfasfa', 'gffa', 'fsafasf', 'fsadfas', 'afsdasfd', 'gfsdsf', 'fgasgadg', '2017-10-03 20:12:33'),
(15, 'a', 'a', 1, 1, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', '2017-10-03 21:15:33'),
(16, 'A', 'A', 2, 1, 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', '2017-10-03 21:21:31'),
(17, 'a', 'a', 1, 1, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', '2017-10-03 21:23:32'),
(18, 'a', 'a', 1, 1, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', '2017-10-03 21:25:56'),
(19, 'a', 'a', 1, 1, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', '2017-10-03 21:44:24'),
(20, 'agfsd', 'dsgds', 2, 42112, 'afd', 'fas', 'sdfg', 'sdg', 'aafs', 'asfdas', 'fasfda', 'sfasf', '2017-10-04 16:38:13'),
(21, 'agfsd', 'dsgds', 2, 42112, 'afd', 'fas', 'sdfg', 'sdg', 'aafs', 'asfdas', 'fasfda', 'sfasf', '2017-10-04 16:39:07'),
(22, 'afafasdf', 'safasdf', 3, 234242, 'afasdf', 'safds', 'sadfasdf', 'afdasfd', 'asfdasdf', 'afdsa', 'dfsd', 'safgs', '2017-10-04 17:58:37'),
(23, 'afas', 'fasdf', 3, 434231, 'afsasdf', 'asdfas', 'gasdads', 'sadfas', 'fasdfa', '21', 'fasdfas', 'safdfas', '2017-10-05 18:13:33'),
(24, 'afdasdf', 'asdfadf', 2, 43121, 'asdfa', 'fhgdfg', 'afdsfasd', 'adsfasfd', 'asdfa', 'asfda', 'sfg', 'fdshdgfdhd', '2017-10-12 21:55:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clinichistory`
--

CREATE TABLE `clinichistory` (
  `id` int(15) NOT NULL,
  `dni_student` int(15) DEFAULT NULL,
  `weight` varchar(5) DEFAULT NULL,
  `size` varchar(5) DEFAULT NULL,
  `chdate` date DEFAULT NULL,
  `id_aquestion` int(15) DEFAULT NULL,
  `pae` varchar(250) DEFAULT NULL,
  `id_diseases` int(15) DEFAULT NULL,
  `home` varchar(3) DEFAULT NULL,
  `TS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clinichistory`
--

INSERT INTO `clinichistory` (`id`, `dni_student`, `weight`, `size`, `chdate`, `id_aquestion`, `pae`, `id_diseases`, `home`, `TS`) VALUES
(1, 1005480532, '12', '12', '2017-09-21', 7, '', 1, 'No', '2017-09-21 19:02:32'),
(2, 1005480532, '123', '123', '2017-09-21', 8, '', 1, 'Yes', '2017-09-21 19:03:21'),
(3, 1001821114, '99889', '89898', '2017-10-02', 11, 'ckmlsdlcksdkml', 1, 'No', '2017-10-03 00:53:35'),
(4, 1001821114, '7876', '7687', '2017-10-02', 13, 'dklsadjaslkdj', 1, 'No', '2017-10-03 01:14:15'),
(5, 1005480532, '1312', '123', '2017-10-03', 14, 'el niño', 1, 'No', '2017-10-03 20:12:33'),
(6, 1001915784, '50', '170', '2017-10-04', 22, 'el adsafad', 1, 'No', '2017-10-04 17:58:37'),
(7, 1005480532, '12', '3232', '2017-10-12', 24, 'el jaja', 1, 'No', '2017-10-12 21:55:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diseases`
--

CREATE TABLE `diseases` (
  `ID` int(15) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `SYMPTOM` varchar(250) DEFAULT NULL,
  `TS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `diseases`
--

INSERT INTO `diseases` (`ID`, `DESCRIPTION`, `SYMPTOM`, `TS`) VALUES
(1, 'Fiebre', '', '2017-10-04 18:17:04'),
(2, 'Alergia', '', '2017-10-12 22:00:54'),
(3, 'Sinusitis', NULL, '2017-10-12 22:01:49'),
(4, 'Problemas estomacales ', NULL, '2017-10-12 22:02:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `students`
--

CREATE TABLE `students` (
  `dni` int(15) NOT NULL,
  `tdni` varchar(15) DEFAULT NULL,
  `grade` varchar(15) DEFAULT NULL,
  `names` varchar(50) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `weight` varchar(15) DEFAULT NULL,
  `size` varchar(3) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `Sex` varchar(2) NOT NULL,
  `phonenum` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `Address` varchar(25) NOT NULL,
  `Bloodtype` varchar(5) NOT NULL,
  `nameparent` varchar(50) DEFAULT NULL,
  `phoneparent` varchar(50) NOT NULL,
  `Eps` varchar(50) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `students`
--

INSERT INTO `students` (`dni`, `tdni`, `grade`, `names`, `firstname`, `weight`, `size`, `birthday`, `Sex`, `phonenum`, `email`, `Address`, `Bloodtype`, `nameparent`, `phoneparent`, `Eps`, `ts`) VALUES
(239655, 'T.I.', '7', 'KEREN SARAY', 'ALMANZA GAMARRA', '', '', '2038-00-01', 'F', '3216326315', '', 'TRANSVERSAL 2 NO. 80-105 ', 'B +', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', 'REGIMEN ESPECIAL DE LA POLICIA', '2017-07-19 17:11:54'),
(40361756, 'NUIP', '6', 'JAIRON XAVIER', 'PARDO ARAUJO', '', '', '0000-00-00', 'M', '3014675976', '', 'CLL 90B N° 6B-67         ', 'O +', 'BIANETH PATRICIA ARAUJO BARRETO', '3013115707', 'SALUD TOTAL', '2017-07-19 17:11:54'),
(104605685, 'T.I.', '7', 'SANTIAGO ', 'CANOLES OLASCOAGA', '', '', '0000-00-00', 'M', '3012614963', '', 'CRA 3A N°91-42           ', 'O +', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', 'COOMEVA', '2017-07-19 17:11:54'),
(1001624432, 'T.I.', '10', 'ANNY YISETH', 'MARTINEZ ANDRADE', '', '', '0000-00-00', 'F', '3137423499', 'xxxx@hotmail.com', 'CLL 94 Nº 5B-07          ', 'O +', 'ALVARO ELIECER MARTINEZ MARTINEZ', '3137423499', 'COOMEVA', '2017-07-19 17:11:46'),
(1001779059, 'T.I.', '11', 'DAVID ALEJANDRO', 'PEREIRA ATENCIA', '', '', '2037-01-02', 'M', '3175579039', '', 'CLL 81 Nº 4A-39          ', '', 'MARY JULY ROLDAN RAMOS', '3004476440', '', '2017-07-19 17:11:47'),
(1001779171, 'T.I.', '10', 'MAURO GABRIEL', 'VILORIA ITURRIAGO', '', '', '0000-00-00', 'M', '3003134242', '', 'CLL 77B Nº               ', '', 'LUIS EDUARDO VILORIA MALDONADO', '3003134242', '                              ', '2017-07-19 17:11:46'),
(1001782087, 'T.I.', '8', 'CLAUDIA MARCELA', 'GUTIERREZ BLANCO', '', '', '0000-00-00', 'F', '3234178384', '', 'CARRERA 3 Nº 88-52       ', 'O +', 'CLAUDIA PATRICIA BLANCO CARBAL', '3234178384', 'CAFE SALUD                    ', '2017-07-19 17:11:56'),
(1001820198, 'T.I.', '8', 'PABLO ANGEL', 'LARIOS VERDOOREN', '', '', '0000-00-00', 'M', '3008160576', 'pablo0723@hotmail.com', 'CRA 9G Nº 80-119         ', 'A +', 'SANDRA PATRICIA VVERDOOREN LOPEZ', '3519757', 'COOMEVA', '2017-07-19 17:11:57'),
(1001820868, 'T.I.', '11', 'MARIA ALEJANDRA', 'MEZA MOLINA', '', '', '0000-00-00', 'F', '3017495981', '', 'CALLE 90 # 3 SUR - 15    ', 'O +', 'DORLIS  MOLINA PEDROZA', '3017495981', 'MUTUAL SER                    ', '2017-07-19 17:11:47'),
(1001820930, 'T.I.', '11', 'LUIS DANIEL', 'TERAN MORENO', '', '', '0000-00-00', 'M', '3145153520', '', 'CLL 94 Nº 6-08           ', 'O +', 'MIRIAM MILENA MORENO BARRERA', '3205716867', 'COOMEVA', '2017-07-19 17:11:47'),
(1001821114, 'T.I.', '11', 'ANTONIO JOSÉ', 'PERALTA BONADIEZ', '', '', '0000-00-00', 'M', '3008847686', '', 'CARRERA 6N # 101 - 25    ', 'B +', 'JACQUELIN CECILIA BONADIEZ CASTILLO', '3008847686', 'CAJACOPI                      ', '2017-07-19 17:11:47'),
(1001822847, 'T.I.', '9', 'JOYMAR MANUEL', 'VILLA BONILLA', '', '', '0000-00-00', 'M', '3015096091', '', 'CALLE 91 # 4 - 07 APARTAM', 'O +', 'MAIRA ALEJANDRA BONILLA BORRERO', '3015096091', 'SELVASALUD                    ', '2017-07-19 17:11:59'),
(1001824220, 'T.I.', '10', 'KEIDDY MARIA', 'GARCIA LOPEZ', '', '', '0000-00-00', 'F', '3126359852', 'xxx@hotmail.com', 'CRA 4C Nº 91-63          ', 'O +', 'DORIS MARIA LOPEZ OLIVERA', '3116595784', 'SALUD TOTAL', '2017-07-19 17:11:47'),
(1001824401, 'T.I.', '9', 'KEIMMY MARIA', 'GARCIA LOPEZ', '', '', '0000-00-00', 'F', '3126359850', '', 'CARRERA 4C#91-63         ', 'B +', 'DORIS MARIA LOPEZ OLIVERA', '3116595784', 'salud total                   ', '2017-07-19 17:11:58'),
(1001824513, 'T.I.', '8', 'LUIS ANGEL', 'CUELLAR BALLESTEROS', '', '', '0000-00-00', 'M', '3017374286', 'daisy_1489@hotmail.com', 'CALLE 51 B # 7 A SUR - 05', 'O +', 'TERESITA DE JESUS BALLESTEROS CORONELL', '3017374285', 'SALUD TOTAL', '2017-07-19 17:11:56'),
(1001824516, 'T.I.', '9', 'GABRIELA ANDREA', 'PARRA LONDOÑO', '', '', '0000-00-00', 'F', '3016461081', 'andrespc1078@hotmail.com', 'CRA 4B Nº 88-109         ', 'O +', 'GREYS KELLY LONDOÑO GOMEZ', '3002927879', 'COOMEVA', '2017-07-19 17:11:58'),
(1001825136, 'T.I.', '8', 'MARIA ALEJANDRA', 'HOLGUIN BELTRAN', '', '', '0000-00-00', 'F', '3004201873', 'malejaholguin@hotmail.com', 'CLL 90 Nº 2-13           ', 'A +', 'MARIA INES BELTRAN DE ORO', '3004201873', 'COMPARTA                      ', '2017-07-19 17:11:56'),
(1001853794, 'T.I.', '10', 'MANUEL SEBASTIAN', 'LEAL CALDERON', '', '', '0000-00-00', 'M', '3017079166', 'calderonrnm@hotmail.com', 'CLL 90 Nº 4C-32          ', 'O +', 'NURYS MARIA CALDERON RANGEL', '3016441530', 'MEDIEPS                       ', '2017-07-19 17:11:46'),
(1001854604, 'T.I.', '8', 'EDWIN LEONARDO', 'VILLARREAL MIRANDA', '', '', '0000-00-00', 'M', '3107446043', 'evil31@hotmail.com', 'CLL 99C Nº 8E-30         ', 'A +', 'EDWIN ENRIQUE VILLARREAL REQUENA', '3107446043', 'SALUD COOP                    ', '2017-07-19 17:11:57'),
(1001854725, 'T.I.', '9', 'BRITHNI JHULIANA', 'VANEGAS OSORIO', '', '', '0000-00-00', 'F', '', '', 'CALLE 99 # 1 SUR - 52    ', 'A +', 'NIXON  VANEGAS RODRIGUEZ', '3217972050', 'MUTUAL SER                    ', '2017-07-19 17:11:58'),
(1001854844, 'T.I.', '9', 'CAROLINA ', 'NAVARRO LOPEZ', '', '', '0000-00-00', 'F', '3002499124', '', 'CRA 2B Nº 66-04          ', 'A +', 'JOHANA DEL CARMEN LOPEZ GOMEZ', '3214504596', 'SALUD TOTAL', '2017-07-19 17:11:58'),
(1001856009, 'T.I.', '8', 'BENJAMIN JUNIOR', 'CANOLES OLASCOAGA', '', '', '0000-00-00', 'M', '3012614963', '', 'CRA 3A Nº 91-42          ', 'O +', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', 'COOMEVA', '2017-07-19 17:11:56'),
(1001856797, 'T.I.', '10', 'SANTIAGO ', 'PERALTA ARDILA', '', '', '0000-00-00', 'M', '3003011424', '', 'CARRERA 6 L2NO. 102-57   ', 'B +', 'LUZ MARINA ARDILA ROBIRA', '3014169047', 'NUEVA EPS                     ', '2017-07-19 17:11:46'),
(1001856994, 'T.I.', '11', 'JORGE ANDRES', 'BORJA SERRANO', '', '', '0000-00-00', 'M', '3003511998', 'kattysofia2011@hotmail.com', 'CLL 94 Nº 4B-26          ', 'A +', 'KATTY SOFIA SERRANO MORENO', '3003511998', 'COOSALUD                      ', '2017-07-19 17:11:47'),
(1001879191, 'T.I.', '11', 'YULEIMA JAZMIN', 'TAFUR GOMEZ', '', '', '0000-00-00', 'F', '3003311317', '', 'CRA 3A Nº 91-62          ', 'O -', 'MARIA ELSY GOMEZ DE MOYA', '3126533683', 'POLICIA NACIONAL              ', '2017-07-19 17:11:48'),
(1001887467, 'T.I.', '10', 'JESUS MIGUEL', 'HERRERA SALAZAR', '', '', '0000-00-00', 'M', '3012027143', '', 'TRANSVERSAL 1B # 76-111  ', 'A +', 'XIMENA DEL CARMEN SALAZAR HOYOS', '3012027143', 'SURA                          ', '2017-07-19 17:11:46'),
(1001893210, 'T.I.', '9', 'ANDREA CAROLINA', 'ARRIETA BROCHERO', '', '', '2037-11-03', 'F', '3013074759', '', 'CRA 2C Nº 88-54          ', '', 'EMILCE ROSARIO CARDENAS CARDENAS', '3013074759', '                              ', '2017-07-19 17:11:57'),
(1001912232, 'T.I.', '10', 'LUIS ANDRES', 'YEPES CARDENAS', '', '', '0000-00-00', 'M', '3024294419', 'noralbacardenas@hotmail.com', 'CRA 1G N° 90-73          ', 'A +', 'NORALBA  CARDENAS CARDENAS', '3024294419', 'SURA                          ', '2017-07-19 17:11:46'),
(1001914895, 'T.I.', '11', 'CARLOS ALBERTO', 'NORIEGA ECHEVERRIA', '', '', '0000-00-00', 'M', '3103512307', 'noriegadecor@gmail.com', 'CR 5E Nº 74B-27          ', 'O +', 'SUGEY ARLIDES ECHEVERRIA DIAZ', '3106512307', 'COMPARTA                      ', '2017-07-19 17:11:47'),
(1001915654, 'T.I.', '9', 'JOSE ALEJANDRO', 'BAYONA OJITO', '', '', '0000-00-00', 'M', '3135383861', 'jbayona401@hotmail.com', 'CRA 3A Nº 91-72          ', 'A +', 'JOSE EMILIO BAYONA SUAREZ', '3135383861', 'COOMEVA', '2017-07-19 17:11:57'),
(1001915784, 'T.I.', '11', 'EDGAR DUVAN', 'AHUMADA URIBE', '', '', '0000-00-00', 'M', '3004709003', '', 'CRA 6F Nº 100-27         ', 'A +', 'DUBYS ELENA SANCHEZ RETAMOZA', '3213164881', '', '2017-07-19 17:11:47'),
(1001916524, 'T.I.', '10', 'KEYNS DAYANA', 'ROMERO MALDONADO', '', '', '0000-00-00', 'F', 'XXXX', 'XXXXX@HOTMAIL.COM', 'CARRERA 3 A # 94 - 80    ', 'O +', 'NELCY LUZ MALDONADO URREGO', '3127011053', 'SALUD TOTAL', '2017-07-19 17:11:46'),
(1001917481, 'T.I.', '9', 'VALERIA SOFIA', 'ARBOLEDA HERRERA', '', '', '0000-00-00', 'F', '3005712318', 'rosaura1429@hotmail.com', 'CRA 3A Nº 92-70          ', 'B +', 'ROSAURA  HERRERA NASSIS', '3005712318', 'POLICIA NACIONAL              ', '2017-07-19 17:11:57'),
(1001918400, 'T.I.', '9', 'JESUS ANTONIO', 'CANO MARIOTA', '', '', '0000-00-00', 'M', '3015089988', 'ladysdecano@gmail.com', 'CARRERA 2 # 91 - 05      ', 'A -', 'LEIDYS PATRICIA MARIOTA HERNANDEZ', '3006430363', 'SISBEN                        ', '2017-07-19 17:11:58'),
(1001918552, 'T.I.', '9', 'JANINNA ISABEL', 'BAZA ARRIETA', '', '', '0000-00-00', 'F', '3017077676', '', 'CARRERA 3 C # 91 - 76    ', 'AB +', 'NAIRA LUZ ARRIETA ORTEGA', '3017077676', 'SALUD TOTAL', '2017-07-19 17:11:58'),
(1001918561, 'T.I.', '10', 'HAMILTON YESID', 'BANQUEZ PASTRANA', '', '', '0000-00-00', 'M', '3005086004', 'XXXX@HOTMAIL.COM', 'CALLE 88 N 2B 14         ', 'A +', 'CANDY PAOLA PASTRANA OYUELA', '3005086004', 'CLINICA DE LA POLICIA         ', '2017-07-19 17:11:46'),
(1001933333, 'T.I.', '8', 'MARIA FERNANDA', 'VELASQUEZ GARCIA', '', '', '0000-00-00', 'F', '3013254458', 'miquecreativo@outlook.com', 'CRA 6E2 Nº 91-58         ', 'O +', 'MIGUEL RAMIRO VELASQUEZ RIOS', '3013254458', 'NUEVA EPS                     ', '2017-07-19 17:11:57'),
(1001940516, 'T.I.', '10', 'KARINA SOFIA', 'GONZALEZ OJEDA', '', '', '0000-00-00', 'F', '3015308115', '', 'CLL 94 Nº 4A-27          ', 'O +', 'KATIA  OJEDA SANDOVAL', '3016197555', 'NUEVA EPS                     ', '2017-07-19 17:11:47'),
(1001942937, 'T.I.', '9', 'YURY MARCELA', 'TORRES HENRIQUEZ', '', '', '0000-00-00', 'F', '3145943053', 'patriciahenriquezgarcia_123@ho', 'CLL 90 Nº 2D-15          ', 'O +', 'SONIA PATRICIA HENRIQUEZ GARCIA', '3145943053', 'COOMEVA', '2017-07-19 17:11:58'),
(1001943375, 'T.I.', '10', 'DIEGO ANDRES', 'ROLONG LOZANO', '', '', '0000-00-00', 'M', '3114294718', '', 'CALLE 98 B # 6 B - 16    ', 'O +', 'MONICA PATRICIA LOZANO LOPEZ', '3205085653', 'CLINICA DE LA POLICIA         ', '2017-07-19 17:11:46'),
(1001944118, 'T.I.', '10', 'CAMILO ANDRES', 'GALINDO MENCO', '', '', '0000-00-00', 'M', '3135407345', '', 'CARRERA 5 SUR # 95 - 20  ', 'O +', 'DOMERITA  MENCO MENDEZ', '3135407345', 'SALUD TOTAL', '2017-07-19 17:11:46'),
(1001945646, 'T.I.', '9', 'VALERIA ', 'MIRANDA TORRENEGRA', '', '', '0000-00-00', 'F', '', '', 'CARRERA 3C # 91 - 76     ', '', 'SHIRLEY DEL CARMEN TORRENEGRA OLIVERO', '3045944328', '', '2017-07-19 17:11:59'),
(1001946085, 'T.I.', '10', 'MICHELLE ARANTXA', 'FLORIAN NAVARRO', '', '', '0000-00-00', 'F', '3145332284', 'SAMUFLOU@HOTMAIL.COM', 'DIAGONAL 88 TRANSVERSAL 1', 'O +', 'NOVELIS DE JESÚS NAVARRO LOPEZ', '3145332284', 'SURA                          ', '2017-07-19 17:11:46'),
(1001946234, 'T.I.', '9', 'LAURA VANESSA', 'LLANOS OROZCO', '', '', '0000-00-00', 'F', '3017497301', '', 'CALLE 94 # 2D - 26       ', 'O -', 'RUBIS ESTHER OROZCO CORTES', '3017497301', 'NUEVA EPS                     ', '2017-07-19 17:11:58'),
(1001946710, 'T.I.', '8', 'SEBASTIAN ', 'CAMARGO MARTINEZ', '', '', '0000-00-00', 'M', '3016364362', 'monik240909@hotmail.com', 'CRA 6A SUR N| 90-56      ', 'O +', 'MONICA PATRICIA MARTINEZ GUERRERO', '3016364362', 'COOMEVA', '2017-07-19 17:11:57'),
(1001947315, 'T.I.', '10', 'MARIA DEL CARMEN', 'ESCOBAR MARRUGO', '', '', '0000-00-00', 'F', '3013159677', 'naesca90@hotmail.com', 'CLL 90 Nº 5-31 APTO 2    ', 'A +', 'NAPOLEON GIL ESCOBAR CALZADA', '3013159677', 'COOMEVA', '2017-07-19 17:11:46'),
(1001947574, 'T.I.', '8', 'JEAN BREYNER', 'FRANCO HERNANDEZ', '', '', '0000-00-00', 'M', '3107470598', '', 'CLL 93 Nº 6-73           ', 'O +', 'HERIBERTO  FRANCO DIAZ', '3107470598', 'XX                            ', '2017-07-19 17:11:56'),
(1001947975, 'T.I.', '8', 'JOSE ANGEL', 'JIMENEZ PADILLA', '', '', '0000-00-00', 'M', '3226491222', '', 'CRA 3A Nº 78- 44         ', 'B -', 'MARIANELA  PADILLA MEDINA', '3147534392', 'COOMEVA', '2017-07-19 17:11:57'),
(1001958942, 'T.I.', '10', 'ELIAN ALBERTO', 'CAMPO ARELLANA', '', '', '0000-00-00', 'M', '3008533083', '', 'CRA 6H Nº 98-17          ', 'O +', 'JOSE ALBERTO CAMPO MANZUR', '3146622219', '', '2017-07-19 17:11:47'),
(1001997387, 'T.I.', '7', 'ISAAC DANIEL', 'CHAVEZ ORTIZ', '', '', '0000-00-00', 'M', '3015859425', '', 'CALLE 99 A # 2 SUR - 09  ', 'A +', 'WILDILMA  ORTIZ ROMERO', '3004833862', 'SALUD COOP                    ', '2017-07-19 17:11:55'),
(1001997922, 'T.I.', '9', 'AILEEN KARINA', 'CASTRO LOPEZ', '', '', '0000-00-00', 'F', '3017897109', 'CASTRO-AILEEN@HOTMAIL.COM', 'CARRERA 5 B # 91-67      ', 'O +', 'MARIA INMACULADA LOPEZ OLIVERA', '3114206425', 'SALUD TOTAL', '2017-07-19 17:11:58'),
(1001998330, 'T.I.', '9', 'LUIS DAVID', 'VERA MEDINA', '', '', '0000-00-00', 'M', '3007596545', '', 'CRA 2 Nº 90-79           ', 'O +', 'MARIA JANETH MEDINA ALVAREZ', '3007596545', 'SURA                          ', '2017-07-19 17:11:59'),
(1001999131, 'T.I.', '10', 'CAMILO ANDRES', 'BARRIOS QUIROZ', '', '', '0000-00-00', 'M', '3135868478', 'albertoalba2003@yahoo.com', 'CLL 76 Nº 5E-28 SANTO DOM', 'A +', 'ALBERTO ENRIQUE BARRIOS BARRIOS', '3135868478', 'salud total                   ', '2017-07-19 17:11:47'),
(1001999196, 'T.I.', '9', 'LUIS DAVID', 'MUNERA PEREZ', '', '', '0000-00-00', 'M', '3118034656', '', 'CALLE 4A Nº 61 27        ', 'A +', 'LUIS ALBERTO MUNERA LOPEZ', '3118034656', 'SURA                          ', '2017-07-19 17:11:58'),
(1001999272, 'T.I.', '7', 'ALDAIR ', 'ATENCIA CAMACHO', '', '', '0000-00-00', 'M', '3042981619', 'ADGRD@HOTMAIL.COM', 'CRA 4A Nº 80-73          ', 'O +', 'AMIRA ISABEL CAMACHO PEÑA', '3042981619', 'COOMEVA', '2017-07-19 17:11:54'),
(1001999279, 'T.I.', '10', 'LAURA VANESSA', 'SERRANO LOPEZ', '', '', '0000-00-00', 'F', '3106439249', '', 'CLL 94 Nº 4B-30          ', 'A +', 'MARTHA LUDIVIA LOPEZ ARAGON', '3106439249', 'COOMEVA', '2017-07-19 17:11:46'),
(1002027132, 'T.I.', '9', 'DAIRO JOSE', 'QUINTANA OLIVARES', '', '', '0000-00-00', 'M', '3103610893', '', 'CARRERA 3C NO. 92-70     ', 'A +', 'YUDIS  OLIVARES MORENO', '3103610893', 'SALUD TOTAL', '2017-07-19 17:11:58'),
(1002030424, 'T.I.', '9', 'VALENTINA ', 'IBAÑEZ MIRANDA', '', '', '0000-00-00', 'F', '3135553533', 'vaime24@hotmail.com', 'CALLE 3C Nº 91-76        ', 'B +', 'MELINA  MIRANDA LEYVA', '3057735926', 'particular                    ', '2017-07-19 17:11:58'),
(1002031194, 'T.I.', '9', 'KEVIN SEBASTIAN', 'PADILLA SARMIENTO', '', '', '0000-00-00', 'M', '3016054557', '', 'CLL 92 Nº 4-31           ', 'O +', 'DIANA MARGARITA SARMEINTO PEREZ', '3014555125', 'SALUD VIDA                    ', '2017-07-19 17:11:58'),
(1002092830, 'T.I.', '10', 'BRANDON SEBASTIAN', 'LLAMAS LARIOS', '', '', '0000-00-00', 'M', '3128815885', '', 'CALLE 104 # 3 SUR - 64   ', 'O +', 'RAQUEL ISABEL LARIOS RIOS', '3128815885', 'CLINICA DE LA POLICIA         ', '2017-07-19 17:11:46'),
(1002132333, 'T.I.', '8', 'JULIO CESAR', 'RIBALDO GAMARRA', '', '', '0000-00-00', 'M', '3004219567', '', 'CALLE 91 # 1F - 23       ', 'O +', 'NARLEN JANETH GAMARRA CASTRO', '3004219567', 'VITAL SALUD                   ', '2017-07-19 17:11:57'),
(1002134734, 'T.I.', '8', 'YUSBELYS DAYANA', 'ARIAS ALVAREZ', '', '', '0000-00-00', 'F', '3017940199', '', 'CRA 4 Nº 92-23           ', 'O +', 'MARIA NADREA ALVAREZ HERRERA', '3104567920', '                              ', '2017-07-19 17:11:56'),
(1002136788, 'T.I.', '9', 'DANIEL ENRIQUE', 'GLEN CARDALES', '', '', '0000-00-00', 'M', '3126202505', 'xxxx@hotmail.com', 'CRA 3SUR Nº 91-77        ', 'O -', 'BETTY MARIA GLEN CARDALES', '3126202505', 'SU SALUD                      ', '2017-07-19 17:11:58'),
(1002152745, 'T.I.', '11', 'HUGO ANDRES', 'TINOCO NOGUERA', '', '', '0000-00-00', 'M', '3164312454', 'hugoandres0323@hotmail.com', 'CALLE 6A# 63-52          ', 'O +', 'DEISY TATIANA NOGUERA MEDINA', '3164312454', 'COOMEVA', '2017-07-19 17:11:47'),
(1002154456, 'T.I.', '11', 'LUIS ANGEL', 'FERNANDEZ ZAMORA', '', '', '2037-00-08', 'M', '3016031525', '', 'CALLE 91 Nº 6B - 40      ', 'O +', 'HEIDY JOHANA ZAMORA SUAREZ', '3193839574', 'SISBEN                        ', '2017-07-19 17:11:47'),
(1002157547, 'T.I.', '11', 'YERSON CAMILO', 'GUZMAN RODRIGUEZ', '', '', '0000-00-00', 'M', '3013962029', 'yerson@hotmail.com', 'CRA 10 SUR Nº 90-47      ', '', 'MARIA ISABEL RODRIGUEZ ORTIZ', '3013962029', '', '2017-07-19 17:11:47'),
(1002159322, 'T.I.', '10', 'ANGEL DAVID', 'BRAVO AGUILAR', '', '', '0000-00-00', 'M', '3118024816', '', 'CALLE 92 # 5 - 23        ', 'O +', 'LUZ MARY AGUILAR SARABIA', '3235987264', 'SALUD COOP                    ', '2017-07-19 17:11:46'),
(1002159554, 'T.I.', '10', 'LAURENIS ', 'MONTENEGRO GONZALEZ', '', '', '0000-00-00', 'F', '3043831589', '', 'CRA 2 SUR Nº 88-04       ', 'O +', 'LINDIS DEL CARMEN GONZALEZ SARMIENTO', '3218280987', 'SALUD COOP                    ', '2017-07-19 17:11:46'),
(1002159837, 'T.I.', '9', 'LINA MARCELA', 'MORON MUÑOZ', '', '', '0000-00-00', 'F', '3135730406', 'maria_mb_19@hotmail.com', 'CRA 7 SUR Nº 98-51       ', 'O +', 'MARIA AUXILIADORA MUÑOZ BELTRAN', '3135551883', 'FAMISANAR                     ', '2017-07-19 17:11:58'),
(1002160329, 'T.I.', '9', 'MARIA ALEJANDRA', 'TEJEDOR CACERES', '', '', '0000-00-00', 'F', '3014748938', 'nubiacm4924@yahoo.com', 'CLL 91 Nº 6B-08          ', 'B +', 'NUBIA ESTHER CACERES MIRANDA', '3014748938', 'COOMEVA', '2017-07-19 17:11:58'),
(1002162661, 'T.I.', '11', 'NATALIA ANDREA', 'PERALTA PEREZ', '', '', '0000-00-00', 'F', '3107341779', '', 'CARRERA 6 P # 102 A - 87 ', 'O +', 'CRISOSOMO  PERALTA HERNANDEZ', '3107341779', 'UNION TEMPORAL DEL NORTE      ', '2017-07-19 17:11:47'),
(1002212685, 'T.I.', '10', 'JESUS ', 'CIFUENTE VIZCAINO', '', '', '0000-00-00', 'M', '3158224237', '', 'CRA 5B Nº 90-27          ', 'O +', 'EVIS DEL CARMEN VIZCAINO SOLANO', '3158224237', '', '2017-07-19 17:11:46'),
(1002227047, 'T.I.', '9', 'VALENTINA MARIA', 'GARCIA TORRES', '', '', '0000-00-00', 'F', '3215721972', 'jgjg29@hotmail.com', 'CRA 2C N° 90-73          ', 'O +', 'RUTHBELIS  TORRES VANEGAS', '3008188527', 'SUBSIDIADA                    ', '2017-07-19 17:11:58'),
(1005480532, 'T.I.', '11', 'DARWIN EFRAIN', 'WANDURRAGA VELASQUEZ', '', '', '0000-00-00', 'M', '3135264392', '', 'CRA 6R Nº 97-119         ', 'A +', 'ROSA MARIA VELASQUEZ SANCHEZ', '3135264392', 'x                             ', '2017-07-19 17:11:47'),
(1007120802, 'T.I.', '10', 'HENDRITH LEONARDO', 'VELASQUEZ MARTELO', '', '', '0000-00-00', 'M', '3126725525', '', 'CALLE 47C # 21C - 24     ', 'O +', 'ERIC LEONARDO VELASQUEZ JARAMILLO', '3126725525', 'SANITAS                       ', '2017-07-19 17:11:47'),
(1007125263, 'T.I.', '8', 'FRANCISCO ANTONIO', 'JIMENEZ JIMENEZ', '', '', '0000-00-00', 'M', '3016529412', '', 'CRA 3A Nº 78-25          ', 'O +', 'MARTHA LUZ JIMENEZ JIMENEZ', '3114051950', 'BARRIOS UNIDOS                ', '2017-07-19 17:11:57'),
(1007175483, 'T.I.', '8', 'CARLOS ALBERTO', 'JIMENEZ AVILA', '', '', '0000-00-00', 'M', '3012132104', '', 'CRA 5E Nº 74-35          ', 'O +', 'HERNAN ALBERTO JIMENEZ CONTRERAS', '3012132104', 'SALUD TOTAL', '2017-07-19 17:11:56'),
(1007892978, 'T.I.', '9', 'LAURA VANESSA', 'LOPEZ HERNANDEZ', '', '', '0000-00-00', 'F', '3006855369', 'norishernandez71@gmail.com', 'CLL 92 N| 3-31           ', 'O -', 'NORIS EDITH HERNANDEZ BOLAÑO', '3006855369', 'SISBEN                        ', '2017-07-19 17:11:58'),
(1007894047, 'T.I.', '9', 'CRISTIAN DE JESUS', 'CAMARGO DE CARO', '', '', '0000-00-00', 'M', '3013622569', '', 'CRA 3B Nº 90-60          ', 'B +', 'RUBY ESTHER DE CARO GUTIERREZ', '3013622569', '', '2017-07-19 17:11:58'),
(1007986973, 'T.I.', '10', 'ELIAN DAVID', 'MONSALVE PEREZ', '', '', '0000-00-00', 'M', '3007439427', '', 'CRA 3A Nº 78-89          ', 'A +', 'YARILIS PAOLA PEREZ PEREZ', '3012667991', 'MUTUAL SER                    ', '2017-07-19 17:11:47'),
(1010122634, 'T.I.', '11', 'VALENTINA ', 'VILLARREAL DE LA HOZ', '', '', '0000-00-00', 'F', '3126690574', '', 'CALLE 91 N° 4 C - 02     ', 'O +', 'BELKIS MARIA DE LA HOZ CABALLERO', '3045953995', 'SALUD TOTAL', '2017-07-19 17:11:48'),
(1016718067, 'NUIP', '2', 'MIGUEL ANTONIO', 'VILLEGAS DOMINGUEZ', '', '', '0000-00-00', 'M', '3014049116', 'emmadmv@hotmail.com', 'CLL 53A TRANSV 1C-15     ', 'B +', 'EMMA ROSA DOMINGUEZ PEÑALOZA', '3014049116', 'COOMEVA', '2017-07-19 17:11:49'),
(1028871163, 'NUIP', '1', 'KANNER IVAN', 'VINASCO GARCIA', '', '', '0000-00-00', 'M', '3042400707', 'kayeesgahu@gmail.com', 'CRA 10B Nº 83-65 LOS ALME', 'O +', 'JEIMMY ESPERANZA GARCIA HUERTAS', '3042400707', 'NUEVA EPS                     ', '2017-07-19 17:11:45'),
(1030559968, 'R.C.', '4', 'EVER SANTIAGO', 'PULGARIN MEDINA', '', '', '0000-00-00', 'M', '3214766246', '', 'CALLE 90 # 3 C - 04      ', 'O +', 'GINNA CAROLINA MEDINA ', '3214766246', 'FAMI SANAR                    ', '2017-07-19 17:11:51'),
(1033979689, 'NUIP', 'JARDIN', 'AXEL STEVAN', 'DURAN RODRIGUEZ', '', '', '0000-00-00', 'M', '3006916859', 'jennys19612010@hotmail.com', 'CLL 91 Nº 2D-35          ', '', 'RODRIGUEZ SALAS JENNYFER PATRICIA', '3006916859', '', '2017-07-19 17:12:00'),
(1037126766, 'R.C.', '1', 'TORIBIO ALBERTO', 'PAUTH MOSQUERA', '', '', '0000-00-00', 'M', '3225067847', '', 'CARRERA 6 B # 89 - 43    ', 'A +', 'ANA MARIA MOSQUERA RUIZ', '3017445419', 'SALUD COOP                    ', '2017-07-19 17:11:46'),
(1041531220, 'T.I.', '7', 'ESTEFANIA ', 'SERNA CARTAGENA', '', '', '0000-00-00', 'F', '3016383588', '', 'CALLE 92 Nº 4 A - 21     ', 'O +', 'BEATRIZ ELENA CARTAGENA CARTAGENA', '3135015470', 'ALUD TOTAL                    ', '2017-07-19 17:11:55'),
(1041691865, 'T.I.', '7', 'PAULA MARGARITA', 'VALDES GUTIERREZ', '', '', '0000-00-00', 'F', '3012855406', 'linagutierrez2007@hotmail.com', 'CRA 3B Nº 90-59          ', 'O +', 'LINA MARGARITA GUTIERREZ MORALES', '3002834504', 'SALUD TOTAL', '2017-07-19 17:11:56'),
(1041692877, 'T.I.', '3', 'AFFETH ', 'LANS HERRERA', '', '', '0000-00-00', 'M', '3014239926', 'ameslans@hotmail.com', 'CALLE 99C # 6E - 51      ', 'A +', 'KATHERIN ISABEL HERRERA AVILA', '3014239926', 'COOMEVA', '2017-07-19 17:11:50'),
(1041694116, 'R.C.', '4', 'CAROLAY ADRIANA', 'PAREDES PEREZ', '', '', '0000-00-00', 'F', '3004262278', '', 'CARRERA 6A # 90 -15      ', 'O +', 'ESTHER EDITH ESCAMILLA ESCAMILLA', '3004262278', 'salu total                    ', '2017-07-19 17:11:51'),
(1041695441, 'NUIP', '3', 'SEBASTIAN ', 'HERNANDEZ GALVIS', '', '', '0000-00-00', 'M', '3016811409', 'julia-2463.jg@hotmail.com', 'CRA 2C N° 91-47          ', 'O +', 'JULIA CRISTINA GALVIS CORTINA', '3016811409', 'CAFE SALUD                    ', '2017-07-19 17:11:50'),
(1041695722, 'NUIP', '2', 'LUZ DIVINA', 'CARBAL SIERRA', '', '', '2040-04-06', 'F', '3218184811', 'luzyouangel-83@hotmail.com', 'CRA 2D N° 88-17          ', 'A +', 'LUZ DIVINA SIERRA BARRIOS', '3218184811', 'SALUD VIDA                    ', '2017-07-19 17:11:48'),
(1041696806, 'R.C.', 'TRANSICION', 'SANTIAGO ', 'LARIOS SANTOS', '', '', '0000-00-00', 'M', '3218508704', 'ELISA1025@HOTMAIL.ES', 'CRA 6J N 96B 16          ', 'O +', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', '', '2017-07-19 17:12:01'),
(1041697437, 'R.C.', 'TRANSICION', 'GUADALUPE ', 'ARISTIZABAL ROMO', '', '', '0000-00-00', 'F', '3003605204', '', 'CARRERA 1 E # 91 -09     ', 'A +', 'XILENA MILAGROS ROMO MONZON', '3308810094', 'COOMEVA', '2017-07-19 17:12:01'),
(1041697503, 'NUIP', 'JARDIN', 'EDIER ANDREZ', 'ARISTIZABAL MARQUEZ', '', '', '2041-02-06', 'M', '3114253954', '', 'CALLE 91 # 1E - 42       ', 'A +', 'CINDY YURANIS MARQUEZ HUYKE', '3114253954', 'SALUD TOTAL', '2017-07-19 17:12:00'),
(1041697588, 'NUIP', 'TRANSICION', 'MATIAS DANIEL', 'CASTILLA ARISTIZABAL', '', '', '2041-07-03', 'M', '3004840456', 'jandy1906@hotmail.com', 'CRA 1E Nº 90-88          ', 'O +', 'DEICY YULIETH ARISTIZABAL ARISTIZABAL', '3004840456', 'XX                            ', '2017-07-19 17:12:01'),
(1041698587, 'NUIP', 'PREJARDIN', 'KIARA PATRICIA', 'GUTIERREZ BLANCO', '', '', '0000-00-00', 'F', '3234178384', 'HERGUTI66@HOTMAIL.COM', 'CRA 3 N 88-52            ', 'O +', 'CLAUDIA PATRICIA BLANCO CARBAL', '3234178384', 'CAFE SALUD                    ', '2017-07-19 17:12:00'),
(1041770681, 'T.I.', '7', 'JHONATHAN STEVEN', 'GARCIA TORRES', '', '', '0000-00-00', 'M', '3008188527', 'jgjg29@hotmail.com', 'CRA 2C N° 90-73          ', 'O +', 'RUTHBELIS  TORRES VANEGAS', '3008188527', 'SUBSIDIADA                    ', '2017-07-19 17:11:55'),
(1041974947, 'T.I.', '7', 'NANCY MARIA', 'VELASQUEZ MARTELO', '', '', '0000-00-00', 'F', '3126725525', '', 'CALLE 47C # 21C - 24     ', 'O +', 'ERIC LEONARDO VELASQUEZ JARAMILLO', '3126725525', 'SANITAS                       ', '2017-07-19 17:11:56'),
(1042240135, 'T.I.', '8', 'MAYRA ALEJANDRA', 'GUTIERREZ ALTAMIRANDA', '', '', '0000-00-00', 'F', '3103569222', 'yal6790@gmail.com', 'CRA 3A Nº 78-127         ', 'A +', 'ADALICIA  MENDOZA CABALLERO', '3103569222', 'COOMEVA', '2017-07-19 17:11:56'),
(1042240609, 'T.I.', '9', 'VALERIA MARIA', 'PINZON BARRIOS', '', '', '0000-00-00', 'F', '3046503873', 'lum-aba@hotmail.com', 'CLL 91 Nº 3A-12          ', '', 'LUZ MARINA BARRIOS BENAVIDES', '', '', '2017-07-19 17:11:58'),
(1042241356, 'T.I.', '8', 'JESUS MANUEL', 'ROBAYO PEREZ', '', '', '2038-07-00', 'M', '3218527670', '', 'CALLE 91 # 5 B - 33      ', 'A +', 'TATIANA ISABEL PEREZ FERNANDEZ', '3004843765', 'SALUD TOTAL', '2017-07-19 17:11:57'),
(1042242796, 'T.I.', '8', 'JUAN DAVID', 'PEREZ VILLALOBOS', '', '', '0000-00-00', 'M', '3008355657', 'xxx@hotmail.com', 'CRA 1G Nº 91-15 APTO 2   ', 'O +', 'NORMA KARINA VILLALOBOS VERGEL', '3008355657', 'COMPARTA                      ', '2017-07-19 17:11:57'),
(1042242945, 'R.C.', '7', 'JOELDY DAVID', 'FLOREZ LOPEZ', '', '', '0000-00-00', 'M', '3007372093', 'michael-263@live.com', 'CARRERA 4 C # 91 - 80    ', 'A +', 'ANA ISABEL LOPEZ OLIVERA', '3007372093', 'SALUD TOTAL', '2017-07-19 17:11:55'),
(1042244844, 'T.I.', '7', 'JUAN DAVID', 'GAMARRA CASTILLO', '', '', '0000-00-00', 'M', '3135412080', '', 'CRA 6A SUR N 90 21       ', 'O +', 'MARJORIE  CASTILLO RUIZ', '3126813099', 'BARRIOS UNIDOS                ', '2017-07-19 17:11:55'),
(1042246974, 'T.I.', '7', 'LANDY ANDREA', 'CARO JIMENEZ', '', '', '0000-00-00', 'F', '3564455 ex', 'luiseduardocaro@hotmail.com', 'CLL 91 Nº 6ASUR-44', 'O +', 'ZENITH DE JESUS PEREZ LOPEZ', '3114269175', 'COOMEVA', '2017-07-19 17:11:55'),
(1042247352, 'T.I.', '6', 'JOSE SEBASTIAN', 'LONDOÑO RAMOS', '', '', '0000-00-00', 'M', '3173868032', 'cenithramos@hotmail.com', 'CLL 54 Nº 3E-76          ', 'O +', 'CENITH ESTER RAMOS POLO', '3012606493', 'COOMEVA', '2017-07-19 17:11:54'),
(1042248283, 'T.I.', '5', 'CECILIA ANDREA', 'BERMUDEZ MARTINEZ', '', '', '0000-00-00', 'F', '3005127591', 'mr33418@gmail.com', 'CLL 94 5-25              ', 'O +', 'MONICA PATRICIA MARTINEZ ROMERO', '3003092942', '', '2017-07-19 17:11:52'),
(1042248284, 'T.I.', '5', 'ASLY CAROLINA', 'BERMUDEZ MARTINEZ', '', '', '0000-00-00', 'F', '3003092942', 'mr33418@gamail.com', 'CLL 94 Nº 5-25           ', 'O +', 'MONICA PATRICIA MARTINEZ ROMERO', '3003092942', 'SALUD COOP                    ', '2017-07-19 17:11:52'),
(1042248542, 'T.I.', '6', 'SOFIA ', 'TERAN MORENO', '', '', '0000-00-00', 'F', '3145153520', 'XXXX@HOTMAIL.COM', 'CLL 94 Nº 6-08           ', 'O +', 'MIRIAM MILENA MORENO BARRERA', '3145153520', 'XXX                           ', '2017-07-19 17:11:54'),
(1042248663, 'T.I.', '7', 'MARIANGEL ', 'OLEA OSPINO', '', '', '0000-00-00', 'F', '3004065107', 'gmospino4@misena.edu.co', 'CRA 1G Nº 91-68 APTO 1   ', 'B +', 'OSCAR OSVALDO OLEA BLANCO', '3004065107', '', '2017-07-19 17:11:55'),
(1042248841, 'T.I.', '6', 'ANA SOFIA', 'PARRA LONDOÑO', '', '', '0000-00-00', 'F', '3016461081', 'andrespc1078@hotmail.com', 'CRA 4 B Nº 88-109        ', 'A +', 'CARLOS ANDREA PARRA CASTRO', '3016461081', 'COOMEVA', '2017-07-19 17:11:54'),
(1042250056, 'T.I.', '5', 'FREDY ESTEBAN', 'CORPAS LOPEZ', '', '', '0000-00-00', 'M', '3042047093', 'eneidalopezorozco@gmail.com', 'CLL 80 Nº 4A-62          ', 'A +', 'ENEIDA  LOPEZ OROZCO', '3042047093', 'COOMEVA', '2017-07-19 17:11:52'),
(1042250139, 'T.I.', '5', 'ARMANDO ANDRÉS', 'GARCÍA ARRIETA', '', '', '0000-00-00', 'M', '3012784655', 'sandra.arrieta14@hotmaill.com', 'CLL 75B Nº 6B-15         ', 'O +', 'SANDRA MILENA ARRIETA ATENCIA', '3012784655', 'COOMEVA', '2017-07-19 17:11:52'),
(1042250157, 'T.I.', '6', 'MARIANA SOFIA', 'ARISTIZABAL CUETO', '', '', '0000-00-00', 'F', '3008809796', 'claudiac1980@hotmail.es', 'CRA 1E Nº 91-09          ', 'AB +', 'CLAUDIA MILENA CUETO BARRIOS', '3008809796', 'COOMEVA', '2017-07-19 17:11:53'),
(1042250318, 'T.I.', '6', 'ZHARICK JOHANA', 'INSIGNARES DELGADO', '', '', '2039-00-03', 'F', '3115832209', '', 'CARRERA 16 A # 77 - 17   ', 'A +', 'SANDRA MILENA DELGADO GUERRERO', '3115832209', 'MAGISTERIO ATLANTICO          ', '2017-07-19 17:11:54'),
(1042250398, 'NUIP', '5', 'MARKO ALEJANDRO', 'ENCISO VANEGAS', '', '', '0000-00-00', 'M', '3107733045', 'xxxx@hotmail.com', 'CLL 99 Nº 2SUR-15        ', 'B +', 'LEYLA  VANEGAS RODRIGUEZ', '3046291633', 'SALUD VIDA                    ', '2017-07-19 17:11:52'),
(1042251861, 'NUIP', '5', 'LAURA CRISTINA', 'URREA PARRA', '', '', '0000-00-00', 'F', '3215542634', '', 'CLL 90 Nº 2C-43          ', 'A +', 'GIANINA PAOLA PARRA DE LA HOZ', '3103626224', 'COOSALUD                      ', '2017-07-19 17:11:53'),
(1042252247, 'T.I.', '5', 'MARIANA ', 'FERNANDEZ SUAREZ', '', '', '2039-05-06', 'F', '3107445336', '', 'CALLE 91 # 6 - 131       ', 'A +', 'ANA ALEXANDRA SUAREZ VANEGAS', '3107445336', 'CAJACOPI                      ', '2017-07-19 17:11:52'),
(1042253066, 'NUIP', '4', 'GUILLERMO ANDRES', 'BERMUDEZ MARTINEZ', '', '', '0000-00-00', 'M', '3003092942', 'mr33418@gmail.com', 'CLL 94 Nº 5-25           ', 'O +', 'MONICA PATRICIA MARTINEZ ROMERO', '3003092942', '', '2017-07-19 17:11:51'),
(1042253374, 'R.C.', '4', 'BREINER DANILSON', 'ARISTIZABAL ROMO', '', '', '0000-00-00', 'M', '3003605204', '', 'CARRERA 1 E # 91 - 09    ', 'A +', 'XILENA MILAGROS ROMO MONZON', '3308810094', 'COOMEVA', '2017-07-19 17:11:51'),
(1042255466, 'NUIP', '4', 'JESHUA YADIEL', 'MONTESINO TAPIA', '', '', '0000-00-00', 'M', '3015278982', 'dayje12@hotmail.com', 'CRA 3C Nº 88-95          ', 'O -', 'DANNY LUZ TAPIA NARVAEZ', '3015278982', 'SALUD TOTAL', '2017-07-19 17:11:52'),
(1042257118, 'T.I.', '4', 'SEBASTIAN ', 'VALDERRAMA HOYOS', '', '', '0000-00-00', 'M', '3017943786', '', 'CARRERA 8 SUR # 97 - 40  ', 'A +', 'JAIME  VALDERRAMA BERNAL', '3017943786', 'SURA                          ', '2017-07-19 17:11:51'),
(1042257148, 'R.C.', '4', 'MARY ANGEL', 'FERNANDEZ SUAREZ', '', '', '0000-00-00', 'F', '3107445336', '', 'CALLE 91 # 6 - 31        ', 'A +', 'ANA ALEXANDRA SUAREZ VANEGAS', '3107445336', 'CAJACOPI                      ', '2017-07-19 17:11:51'),
(1042257322, 'T.I.', '3', 'JOSE LUIS', 'BERTEL POLO', '', '', '0000-00-00', 'M', '3165254577', '', 'CRA 3A N 92-49           ', 'O +', 'SAMIRA ESTHER POLO MENDOZA', '3165254577', 'SALUD TOTAL', '2017-07-19 17:11:49'),
(1042257946, 'R.C.', '3', 'LUCIANA MARGARITA', 'MEJIA MEJIA', '', '', '0000-00-00', 'F', '3008859748', '', 'CARRERA 1F # 91 - 15     ', 'O +', 'JOHANA MARCELA MEJIA PALENCIA', '3008859748', 'COMPARTA                      ', '2017-07-19 17:11:49'),
(1042260526, 'NUIP', '2', 'YHARA MARGARITA', 'VASQUEZ FERRER', '', '', '2040-07-08', 'F', '3016875326', '', 'CARRERA 6 N Nº 101-73    ', 'A +', 'KIARA MARGARITA FERRER DIAZ', '3016875326', 'MUTUALSER                     ', '2017-07-19 17:11:49'),
(1042262124, 'R.C.', '2', 'MATIAS ', 'CERVANTES PEREZ', '', '', '0000-00-00', 'M', '3168784711', '', 'CALLE 94 # 3B - 33 APTO 1', 'O +', 'KELLIS ESTHER PEREZ MATOS', '3195400185', 'SURA                          ', '2017-07-19 17:11:48'),
(1042264814, 'R.C.', 'JARDIN', 'ALEJANDRO ', 'AVILA DE LA ROSA', '', '', '0000-00-00', 'M', '3003441904', '', 'CARRERA 5 # 94 - 55      ', 'O +', 'NARLY JUDITH DE LA ROSA ARROYO', '3003441904', 'COOMEVA', '2017-07-19 17:11:59'),
(1042578201, 'T.I.', '8', 'AARON ', 'CABRALES CONTRERAS', '', '', '0000-00-00', 'M', '', '', 'CALLE 92 # 5 - 40        ', 'O +', 'IGNASIA LUCIA CONTRERAS HERNANDEZ', '3013253119', 'SALUD TOTAL', '2017-07-19 17:11:57'),
(1042848198, 'T.I.', '8', 'DYLAN MAURICIO', 'MONTES PEREA', '', '', '0000-00-00', 'M', '3208301071', 'dylanmau@hotmail.com', 'CLL 88 N° 4A-44          ', 'A +', 'AMIRA DE JESUS ATENCIA ATENCIA', '3012686089', 'SURA EPS                      ', '2017-07-19 17:11:56'),
(1042849500, 'T.I.', '8', 'LEIDY ISABEL', 'BERDUGO MARTINEZ', '', '', '0000-00-00', 'F', '3003760651', 'yese828@hotmail.com', 'CLL 79 N| 20A-10         ', 'AB +', 'BESLY YESENIA MARTINEZ CASTAÑEDA', '3003760651', 'MEDIEPS                       ', '2017-07-19 17:11:57'),
(1042851141, 'T.I.', '6', 'JUAN CAMILO', 'FERNANDEZ BARRIOS', '', '', '0000-00-00', 'M', '3005764829', '', 'CALLE 95 # 3 SUR - 04    ', 'A +', 'CARMEN  BARRIOS MEDINA', '3014129343', 'SALUD TOTAL', '2017-07-19 17:11:53'),
(1042851151, 'T.I.', '6', 'SAMUEL CAMILO', 'SAMPAYO GONZALEZ', '', '', '0000-00-00', 'M', '3016307056', 'mayg1203@gamil.com', 'CLL 92 Nº 4B-32          ', 'O +', 'MAYRA MERCEDES GONZALEZ DE LA HOZ', '3016307056', 'COOMEVA', '2017-07-19 17:11:54'),
(1042851306, 'NUIP', '5', 'CAMILO ANDRES', 'GUERRA HERRERA', '', '', '0000-00-00', 'M', '3005712318', '', 'CRA 3A Nº 92-70          ', 'O +', 'ROSAURA  HERRERA NASSIS', '3005712318', '                              ', '2017-07-19 17:11:52'),
(1042852118, 'T.I.', '5', 'MARIA CLAUDIA', 'PERALTA PEREZ', '', '', '0000-00-00', 'F', '3107341779', '', 'CARRERA 6P # 102A-87     ', 'O +', 'CRISOSOMO  PERALTA HERNANDEZ', '3107341779', 'UNION TEMPORAL DEL NORTE      ', '2017-07-19 17:11:53'),
(1042852421, 'T.I.', '6', 'ANGEL DAVID', 'RUIZ ORTEGA', '', '', '2039-04-08', 'M', '3002167466', 'martha-0rtega24@hotmail.com', 'CRA 4A Nº 91-19          ', 'O +', 'MARTHA  ORTEGA MURILLO', '3002167466', 'COOMEVA', '2017-07-19 17:11:54'),
(1042853169, 'NUIP', '6', 'MARGARITA SAUDITH', 'OSPINO DE LOS REYES', '', '', '2039-09-07', 'F', '3216662903', 'djadibys@yahoo.es', 'CRA 4 N| 94-50           ', 'O +', 'OSPINO UTRIA CARLOS ALBERTO', '3216662903', 'SALUD TOTAL', '2017-07-19 17:11:54'),
(1042853883, 'T.I.', '4', 'MARIA ANGEL', 'VILLARREAL DE LA HOZ', '', '', '0000-00-00', 'F', '3045953995', 'mariadelahoz7733@hotmail.com', 'CALLE 90 N° 4C - 01      ', 'B +', 'BELKIS MARIA DE LA HOZ CABALLERO', '3045953995', 'SALUD TOTAL', '2017-07-19 17:11:51'),
(1042854024, 'NUIP', '4', 'GENESIS PATRICIA', 'CAMPO ARELLANA', '', '', '0000-00-00', 'F', '3008533083', '', 'CRA 6H Nº 98-17          ', 'O +', 'JOSE ALBERTO CAMPO MANZUR', '3146622219', 'SALUD COOP                    ', '2017-07-19 17:11:50'),
(1042856041, 'NUIP', '3', 'DIEGO ENRIQUE', 'RODRIGUEZ GIL', '', '', '0000-00-00', 'M', '3057710834', '', 'CLL 91 Nº 4-21           ', 'O +', 'LICETH MARIA GIL PEREZ', '3226961842', 'COOMEVA', '2017-07-19 17:11:50'),
(1042857400, 'NUIP', '2', 'ALEJANDRO DAVID', 'RICAURTE COLMENARES', '', '', '0000-00-00', 'M', '3103626224', 'lindacolmenares0609@hotmail.co', 'CRA 3B Nº 90-49          ', 'A +', 'LINDA STEFANY COLMENARES VERA', '3103626224', 'SALUD COOP                    ', '2017-07-19 17:11:49'),
(1042858262, 'NUIP', '2', 'MATIAS DAVID', 'OLMOS GOMEZ', '', '', '0000-00-00', 'M', '3014085102', 'ronaldvolmos28@hotmail.com', 'CRA 4B N° 88-125         ', 'O +', 'RONALD OSVALDO OLMOS VILLA', '3014085102', 'COOMEVA', '2017-07-19 17:11:48'),
(1042859538, 'NUIP', '1', 'ISABELLA ANDREA', 'CASTRO PINO', '', '', '0000-00-00', 'F', '3116881189', 'ameliapino@hotmail.com', 'CRA 3C Nº 88-85          ', 'A +', 'RUBIELA  ECHEVERRI DE CASTRO', '3288224', 'COOMEVA', '2017-07-19 17:11:45'),
(1042860482, 'R.C.', 'TRANSICION', 'CHEILA JOHANA', 'TRUJILLO GONZALEZ', '', '', '0000-00-00', 'F', '3217703708', '', 'CARRERA 7 SUR # 90 - 17  ', 'B +', 'SHIRLY MARIA GONZALEZ VILORIA', '3217703708', 'SURA                          ', '2017-07-19 17:12:02'),
(1042861036, 'NUIP', 'TRANSICION', 'MELANY ', 'OSPINO VELASQUEZ', '', '', '0000-00-00', 'F', '3015079749', '', 'CARRERA 6J Nº 98-80      ', 'O +', 'DAIRON JOSE RAMOS SANCHEZ', '301437936', 'Coomeva                       ', '2017-07-19 17:12:02'),
(1043114082, 'T.I.', '10', 'LORAINE ANDREA', 'SANTIZ CORONELL', '', '', '0000-00-00', 'F', '3163367392', 'alvarosantiz@canguro.com.co', 'CRA 3 Nº 88-99 APTO 1    ', 'O +', 'ALVARO ENRIQUE SANTIZ CUDRIZ', '3163367392', 'COOMEVA', '2017-07-19 17:11:46'),
(1043121445, 'T.I.', '7', 'SHARICK ', 'LLAMAS LARIOS', '', '', '0000-00-00', 'F', '3128815885', '', 'CALLE 104 # 3 SUR - 64   ', 'O +', 'RAQUEL ISABEL LARIOS RIOS', '3128815885', 'CLINICA DE LA POLICIA         ', '2017-07-19 17:11:55'),
(1043146150, 'T.I.', '7', 'MICHELL GABRIELA', 'BOLAÑO VELASQUEZ', '', '', '0000-00-00', 'F', '3145235171', '', 'CARRERA 3 A # 94 - 70    ', '', 'DIANA ESTHER BOLAÑO PEDRAZA', '3145235171', 'XXX                           ', '2017-07-19 17:11:55'),
(1043162053, 'NUIP', '1', 'SHAROLCK ', 'LLAMAS LARIOS', '', '', '0000-00-00', 'F', '3128815885', 'deysonllamas@hotmail.com', 'CLL 104 N° 3SUR-64       ', 'O +', 'RAQUEL ISABEL LARIOS RIOS', '3128815885', 'POLICIA NACIONAL              ', '2017-07-19 17:11:45'),
(1043162211, 'R.C.', 'TRANSICION', 'LUZ MARINA', 'FERNANDEZ VALENZUELA', '', '', '0000-00-00', 'F', '3135412080', 'MARGORIECASTILLO344@HOTMAIL.CO', 'CRA 6A SUR N 90 21       ', '', 'MARJORIE  CASTILLO RUIZ', '3126813099', '', '2017-07-19 17:12:01'),
(1043178210, 'NUIP', 'PREJARDIN', 'STEPHANIA ', 'VILLALOBOS MARIN', '', '', '0000-00-00', 'F', '3007017592', '', 'CRA 1G N 91-40           ', 'O +', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', 'LA NUEVA EPS                  ', '2017-07-19 17:12:01'),
(1043434117, 'T.I.', '9', 'YERIBETH MARIA', 'PALENCIA BOLIVAR', '', '', '0000-00-00', 'F', '3003146092', 'vvvvv@hotmail.com', 'CRA 5C Nº 92-79          ', '', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', '', '2017-07-19 17:11:59'),
(1043434528, 'T.I.', '10', 'JONATHAN JOSÉ', 'VARGAS DELGADO', '', '', '0000-00-00', 'M', '3115832209', '', 'CARRERA 16 A # 77 - 17   ', 'O +', 'SANDRA MILENA DELGADO GUERRERO', '3115832209', 'MAGISTERIO ATLANTICO          ', '2017-07-19 17:11:46'),
(1043435984, 'T.I.', '6', 'HERNAN ALBERTO', 'JIMENEZ AVILA', '', '', '0000-00-00', 'M', '3012132104', '', 'CRA 5E Nº 74-35          ', 'O +', 'HERNAN ALBERTO JIMENEZ CONTRERAS', '3012132104', 'SALUD TOTAL', '2017-07-19 17:11:54'),
(1043437292, 'T.I.', '6', 'JESUS DAVID', 'JIMENEZ AVILA', '', '', '0000-00-00', 'M', '3012132104', '', 'CRA 5E Nº 74-35          ', 'O +', 'HERNAN ALBERTO JIMENEZ CONTRERAS', '3012132104', 'SALUD TOTAL', '2017-07-19 17:11:54'),
(1043437303, 'T.I.', '7', 'REYNALDO ', 'CALA DE LA ROSA', '', '', '0000-00-00', 'M', '3218343381', '', 'CRA 6L Nº 97-102', 'O +', 'REYNALDO  CALA ', '3218343381', 'SISBEN                        ', '2017-07-19 17:11:55'),
(1043439221, 'T.I.', '6', 'JUAN ESTEBAN', 'ACOSTA VENEGAS', '', '', '0000-00-00', 'M', '3014413131', '', 'CALLE 75 A # 6 - 27 APTO ', 'O +', 'MARIA DEL CARMEN VENEGAS CAMARGO', '3193937955', 'COOMEVA', '2017-07-19 17:11:53'),
(1043439807, 'T.I.', '6', 'ANNDY ', 'GARCIA LONDOÑO', '', '', '0000-00-00', 'M', '3042186423', 'papyeddy@hotmail.es', 'CRA 4B Nº 88-153         ', 'O +', 'JOICE ESTHER LONDOÑO GOMEZ', '3042180423', 'SALUD COOP                    ', '2017-07-19 17:11:53'),
(1043441393, 'T.I.', '6', 'WEINCES NAYID', 'HERNANDEZ PEREZ', '', '', '0000-00-00', 'M', '3168784711', '', 'CALLE 94 # 3B - 33 APTO 2', 'O +', 'KELLIS ESTHER PEREZ MATOS', '3195400185', 'SURA                          ', '2017-07-19 17:11:53'),
(1043442056, 'T.I.', '6', 'MARIA FERNANDA', 'HERRERA MANZANO', '', '', '0000-00-00', 'F', '3002566653', '', 'CLL 90 Nº 6H-91          ', 'A +', 'NADIA PATRICIA MANZANO PUENTES', '3002566653', '', '2017-07-19 17:11:54'),
(1043443205, 'T.I.', '5', 'VALERY LILIANA', 'SANABRIA GALVIS', '', '', '2039-06-05', 'F', '3135454913', '', 'CARRERA 5 SUR # 96 - 100 ', 'AB +', 'ALICIA  GALVIS MARTINEZ', '3135454913', 'SALUD TOTAL', '2017-07-19 17:11:53'),
(1043445144, 'R.C.', '4', 'SEBASTIAN ', 'SALCEDO CERVANTES', '', '', '0000-00-00', 'M', '3008764831', 'handkyseb27@hotmail.com', 'CRA 3C Nº 92-39          ', 'A +', 'HAROLD MANUEL SALCEDO ROJAS', '3008764831', 'SALUD TOTAL', '2017-07-19 17:11:52'),
(1043445405, 'T.I.', '5', 'MATEO ANDRES', 'TORRES SANTIS', '', '', '2039-04-08', 'M', '3014201383', '', 'CALLE 92 3 5B - 35       ', 'O +', 'JOHANA ISABEL SANTIS SERPA', '3012524870', 'SALUD COOP                    ', '2017-07-19 17:11:53'),
(1043445799, 'NUIP', '4', 'EDIER DE JESUS', 'PEÑA JIMENO', '', '', '0000-00-00', 'M', '3004000220', 'maritzajimeno@hotmail.com', 'CRA 2C Nº 88-110         ', 'O +', 'MARITZA  JIMENO PEREZ', '3004000220', 'SALUD TOTAL', '2017-07-19 17:11:52'),
(1043445855, 'T.I.', '4', 'HANSEL DAVID', 'AGRESOT MEDINA', '', '', '0000-00-00', 'M', '3004383819', '', 'CLL 91 Nº 4-92 PISO 2    ', 'A +', 'MARTA INES MEDINA HERRERA', '3004383819', 'SALUD TOTAL', '2017-07-19 17:11:51'),
(1043446495, 'T.I.', '4', 'SEBASTIAN ANDRES', 'MIRANDA NIÑO', '', '', '0000-00-00', 'M', '3188007602', 'yennigut18@hotmail.com', 'CRA 2 SUR Nº 91-04       ', 'O +', 'YENDRI CATALINA NIÑO GUTIERREZ', '3188007602', 'SURA                          ', '2017-07-19 17:11:51'),
(1043447609, 'R.C.', '4', 'HILARY SOFIA', 'ACUÑA VERGARA', '', '', '0000-00-00', 'F', '3205505582', '', 'CARRERA 3 # 91 - 42      ', 'A +', 'MARTIN ENRIQUE SIMANCA ACUÑA', '3218920698', 'SISBEN                        ', '2017-07-19 17:11:50'),
(1043449705, 'R.C.', '2', 'EMMANUEL JESUS', 'CANO MARIOTA', '', '', '0000-00-00', 'M', '3015089988', '', 'CARRERA 2 # 91 - 05      ', 'O +', 'LEIDYS PATRICIA MARIOTA HERNANDEZ', '3006430363', 'SISBEN                        ', '2017-07-19 17:11:48'),
(1043451045, 'NUIP', '3', 'KESMAN JORETH', 'FLOREZ DIAZ', '', '', '0000-00-00', 'M', '3004323083', 'marydiaz1014@hotmail.com', 'CLL 99 Nº 6-170          ', 'A +', 'MARY BELQUIS DIAZ FUENTES', '3004323083', 'CAFESALUD                     ', '2017-07-19 17:11:50'),
(1043451156, 'NUIP', '3', 'ALEJANDRO JOSE', 'MEJÍA ROLONG', '', '', '0000-00-00', 'M', '3015869920', 'danitza_rolong_2209@hotmail.co', 'CRA 5B Nº 92-51          ', 'O +', 'MARGARITA VICTORIA ROLONG PACHECO', '3015869920', 'SALUD TOTAL', '2017-07-19 17:11:50'),
(1043451239, 'R.C.', '3', 'VALENTINA SOFIA', 'TIRIA JIMENEZ', '', '', '0000-00-00', 'F', '3164915347', '', 'CALLE 90 B # 6 H - 106   ', 'O +', 'SARAY VALENTINA FERNADEZ DE JIMENEZ', '3185813219', 'COOMEVA', '2017-07-19 17:11:50'),
(1043451414, 'R.C.', '3', 'HELLEN JOHANA', 'TORRES SANTIS', '', '', '0000-00-00', 'F', '3002076096', '', 'CALLE 92 # 5B - 35       ', 'A +', 'JOHANA ISABEL SANTIS SERPA', '3012524870', 'SALUD COOP                    ', '2017-07-19 17:11:50'),
(1043451531, 'NUIP', '3', 'CAMILO ANDRES', 'CRUZ GONZALEZ', '', '', '2040-02-08', 'M', '3135976522', 'lusgoqui@hotmail.com', 'CRA 6A Nº 94-36          ', 'O +', 'LUZ STELLA GONZALEZ QUIMBAYO', '3135976522', 'CLINICA DE LA POLICIA         ', '2017-07-19 17:11:49'),
(1043452117, 'NUIP', '2', 'ISABELLA ANDREA', 'JIMENEZ PATRON', '', '', '2040-08-08', 'F', '3135385524', 'hhhhh@hotmail.com', 'CLL 92 Nº 3A-50          ', 'O +', 'VICTORIA ISABEL PATRON ANILLO', '3135385524', 'CAJACOPI                      ', '2017-07-19 17:11:48'),
(1043452392, 'NUIP', '1', 'JHOSUA ', 'ECHEVERRIA RIVERA', '', '', '2040-11-01', 'M', '3046680980', 'socadellevalentinacorcho@gmail', 'CRA 3C Nº 92-69          ', 'O +', 'FRANCIA ELENA RIVERA PEREZ', '3046680980', 'COOMEVA', '2017-07-19 17:11:46'),
(1043453795, 'NUIP', '1', 'BRIAN SMITH', 'CAMARGO FONTALVO', '', '', '0000-00-00', 'M', '3012940260', 'angiepao96@hotmail.es', 'CRA 6 Nº 75A-04          ', 'O +', 'MARIA FERNANDA FONTALVO VARGAS', '3012940260', '', '2017-07-19 17:11:46'),
(1043454713, 'R.C.', '1', 'ANNYES VALENTINA', 'MARQUEZ RODRIGUEZ', '', '', '0000-00-00', 'F', '3126759215', '', 'CALLE 90 # 4SUR - 17     ', 'B +', 'ADRIANA ISABEL RODRIGUEZ SANCHEZ', '3126759215', 'CAFESALUD                     ', '2017-07-19 17:11:45'),
(1043455102, 'NUIP', '1', 'ALEJANDRA SOFIA', 'MARIMON GRANADOS', '', '', '0000-00-00', 'F', '3012467565', 'alexandrag3009@hotmail.com', 'CLL 94 Nº 3-27           ', 'O +', 'ALEXANDRA MILENA GRANADOS DE AVILA', '3043389651', 'SALUD COOP                    ', '2017-07-19 17:11:45'),
(1043456800, 'NUIP', '1', 'SEBASTIAN ANDRES', 'SABALZA HERRERA', '', '', '0000-00-00', 'M', '3006688735', 'johor23@hotmail.com', 'CRA 3A Nº 92-70          ', 'B +', 'LEDIS  HERRERA NASSIS', '3006688735', 'SALUD TOTAL', '2017-07-19 17:11:46'),
(1043456859, 'NUIP', 'TRANSICION', 'MATIAS ALEXIS', 'PADILLA IGLESIAS', '', '', '0000-00-00', 'M', '3017415069', '', 'CARRERA 4C Nª 99 - 20    ', 'A +', 'ERIKA  ESCAMILLA ESCAMILLA', '3003878547', 'SURA EPS                      ', '2017-07-19 17:12:01'),
(1043457144, 'R.C.', 'TRANSICION', 'ISABELLA SOFIA', 'SALCEDO CERVANTES', '', '', '0000-00-00', 'F', '3008764831', 'handkyseb27@hotmail.com', 'CARRERA 3 C # 92 - 39    ', 'A +', 'HAROLD MANUEL SALCEDO ROJAS', '3008764831', 'SALUD TOTAL', '2017-07-19 17:12:01'),
(1043457257, 'NUIP', '1', 'ALEJANDRA ISABEL', 'MENDOZA SOTO', '', '', '0000-00-00', 'F', '3004066299', 'yysotop@hotmail.com', 'CLL 80 Nº 1B-02          ', 'O +', 'YARLIN YORLANIS SOTO PERALTA', '3004066299', 'SALUD TOTAL', '2017-07-19 17:11:45'),
(1043458591, 'NUIP', 'TRANSICION', 'HASSAN JOSEPH', 'GONZALEZ BUITRAGO', '', '', '2041-00-01', 'M', '3013717057', 'jamabacu@hotmail.com', 'CRA 3A Nº 92-81          ', 'O +', 'YANETH MARIA BAZA CUBILLOS', '3013717057', 'POLICIA NACIONAL              ', '2017-07-19 17:12:01'),
(1043461836, 'R.C.', 'JARDIN', 'MAXILIMIANO ', 'TOVAR SAÑUDO', '', '', '0000-00-00', 'M', '3015246601', 'MAIRASALOP@HOTMAIL.COM', 'CALLE 90 # 6B - 56       ', 'O +', 'MAIRA ALEJANDRA SAÑUDO LOPEZ', '3002689063', 'SURA                          ', '2017-07-19 17:12:00'),
(1043462157, 'NUIP', 'JARDIN', 'FABIAN ZAID', 'PAREDES PEREZ', '', '', '0000-00-00', 'M', '3003148833', '', 'CARRERA 6A Nº 90 - 15    ', 'O +', 'ESTHER EDITH ESCAMILLA ESCAMILLA', '3004262278', 'SALUD TOTAL', '2017-07-19 17:12:00'),
(1043464707, 'NUIP', 'PREJARDIN', 'FREDDY JOSE', 'ROBAYO PEREZ', '', '', '0000-00-00', 'M', '3004843765', 'tato0324@hotmail.es', 'CALLE 91 5 B 33          ', '', 'TATIANA ISABEL PEREZ FERNANDEZ', '3004843765', '', '2017-07-19 17:12:00'),
(1043464867, 'R.C.', 'PREJARDIN', 'BRIANNA ', 'GUZMAN IGLESIAS', '', '', '0000-00-00', 'F', '3012529714', 'yeraliglesiasc@hotmail.com', 'CARRERA 4 SUR # 104 - 24 ', 'A +', 'YERALDINE  IGLESIAS CARABALLO', '3017542694', 'SALUD TOTAL', '2017-07-19 17:12:00'),
(1043465961, 'NUIP', 'PREJARDIN', 'DIEGO ANDRÉS', 'LOPEZ BARRIOS', '', '', '0000-00-00', 'M', '3006722457', '', 'CALLE 98 C NO. 3C-13, CON', 'O +', 'YURI PAOLA BARRIOS LONDOÑO', '3006722457', 'SURA                          ', '2017-07-19 17:12:00'),
(1043475291, 'T.I.', '3', 'SILFREDO ANTONIO', 'MONTENEGRO OJEDA', '', '', '0000-00-00', 'M', '3004138029', 'ivanmadridojeda@outlook.es', 'CALLE 96#3C-06           ', 'B +', 'CECILIA ELENA OJEDA ELGUEDO', '3004138029', 'saludcoop                     ', '2017-07-19 17:11:50'),
(1043661699, 'NUIP', '7', 'KEVIN ', 'RODRIGUEZ GONZALEZ', '', '', '2038-07-04', 'M', '3135400106', '', 'GARDENIAS. CONJUNTO 5. TO', 'O +', 'MICHELLE STEPHANY RODRIGUEZ GONZALEZ', '', 'Sura                          ', '2017-07-19 17:11:56'),
(1043662491, 'T.I.', '8', 'JUAN CAMILO', 'CARBONELL HEILBRON', '', '', '0000-00-00', 'M', '3205136463', '', 'CALLE 95 # 6-73          ', 'O +', 'ELMUS ANTONIO CARBONELL ORTEGA', '3205136463', 'NUEVA EPS                     ', '2017-07-19 17:11:57'),
(1043663513, 'T.I.', '8', 'YORLEIDYS SARAY', 'HERRERA MORALES', '', '', '0000-00-00', 'F', '3287510', 'yorley1023@hotmail.com', 'CRA 6C1 Nº 89-09         ', 'A +', 'LUIS ANTONIO HERRERA FIGUEROA', '3215346111', 'COOMEVA', '2017-07-19 17:11:56'),
(1043663777, 'NUIP', '6', 'RAFAEL HERNANDO', 'LLANOS MUGNO', '', '', '0000-00-00', 'M', '3012357164', '', 'CALLE 78 Nº 1D-64        ', 'A +', 'KARLA PATRICIA MUGNO ALVAREZ', '3046672394', 'SAlUD TOTAL                   ', '2017-07-19 17:11:54'),
(1043666109, 'T.I.', '5', 'JUAN CAMILO', 'RAMIREZ QUINTERO', '', '', '0000-00-00', 'M', '3206501610', '', 'CALLE 94 # 3 A - 48      ', 'A +', 'ISMELDA ROSA QUINTERO TRUJILLO', '3206501610', 'SALUD TOTAL', '2017-07-19 17:11:53'),
(1043668683, 'T.I.', '5', 'CLAUDIA PATRICIA', 'OROZCO BELTRAN', '', '', '0000-00-00', 'F', '3288498', 'mariaorozco66@hotmail.com', 'CRA 3A Nº 92-50          ', 'A +', 'MARIA ISABEL OROZCO CORTES', '3174676711', 'XX                            ', '2017-07-19 17:11:53'),
(1043671269, 'T.I.', '5', 'LIZDANIS PAOLA', 'MOLINA IZQUIERDO', '', '', '0000-00-00', 'F', '3012007474', 'graneropaola@hotmail.com', 'CRA 1 Nº 90-95           ', 'O +', 'DAILYS MERCEDES IZQUIERDO MONTESINO', '3012007474', 'XXX                           ', '2017-07-19 17:11:53'),
(1043672548, 'T.I.', '8', 'KELSY MARIA', 'FONTALVO VILLANUEVA', '', '', '2038-02-09', 'F', '3008982786', 'xxxx@gmail.com', 'CLL 90 Nº 2C-44          ', 'A +', 'TOMASA MARIA MARTELO MARRIAGA', '3008982786', 'SALUD VIDA                    ', '2017-07-19 17:11:57'),
(1043673842, 'R.C.', '4', 'JONATHAN DAVID', 'FLOREZ RAMBAO', '', '', '0000-00-00', 'M', '3148199235', '', 'CARRERA 3C # 92 - 30     ', 'B +', 'YARELLIS ESTHER RAMBAO OLIVO', '3148199235', 'COOMEVA', '2017-07-19 17:11:51'),
(1043675116, 'NUIP', '4', 'ALBERTO ESTEBAN', 'VASQUEZ POLO', '', '', '0000-00-00', 'M', '3188337893', '', 'CLL 92 Nº 4-19           ', 'B +', 'BELQUIS MARIA POLO ARRIETA', '3188337893', 'SALUD TOTAL', '2017-07-19 17:11:52'),
(1043676058, 'NUIP', '3', 'MARIANA SOFIA', 'POLO MIRANDA', '', '', '0000-00-00', 'F', '3135553533', 'janethleyva17@gmail.com', 'CALLE 3C Nº 91-76        ', 'B -', 'MELINA  MIRANDA LEYVA', '3057735926', 'COOMEVA', '2017-07-19 17:11:50'),
(1043676251, 'NUIP', '4', 'VALERY DANIELA', 'DIAZ IZQUIERDO', '', '', '0000-00-00', 'F', '3012007474', 'graneropaola@hotmail.com', 'CRA 1 Nº 90-95           ', 'A +', 'DAILYS MERCEDES IZQUIERDO MONTESINO', '3012007474', 'SALUD TOTAL', '2017-07-19 17:11:51'),
(1043676289, 'NUIP', '4', 'BRIGEETH CAMILA', 'CAMPO RODRIGUEZ', '', '', '0000-00-00', 'F', '3007254698', 'XX@HOTMAIL.COM', 'CLL 72C Nº 23C-12        ', 'O +', 'YANETH SUSANA RODRIGUEZ MARTINEZ', '3007254698', 'XXX                           ', '2017-07-19 17:11:51');
INSERT INTO `students` (`dni`, `tdni`, `grade`, `names`, `firstname`, `weight`, `size`, `birthday`, `Sex`, `phonenum`, `email`, `Address`, `Bloodtype`, `nameparent`, `phoneparent`, `Eps`, `ts`) VALUES
(1043676827, 'R.C.', '3', 'LIA VALENTINA', 'VILLALOBOS VERGEL', '', '', '0000-00-00', 'F', '3007017592', '', 'CARRERA 1 G # 91 - 44    ', 'O +', 'MARTHA CECILIA VERGEL DE VILLALOBOS', '3007017592', 'SOLSALUD                      ', '2017-07-19 17:11:50'),
(1043677221, 'NUIP', '4', 'SANTIAGO DANIEL', 'PEDROZO TAPIA', '', '', '0000-00-00', 'M', '3014127931', 'iristapiasn@gmail.com', 'CLL 94 Nº 2D-49          ', 'B +', 'IRIS JASLEIDIS TAPIA NIÑO', '3014127931', 'MUTUAL SER                    ', '2017-07-19 17:11:52'),
(1043678254, 'R.C.', '3', 'MELANIE SOFIA', 'BARBOSA LEAL', '', '', '0000-00-00', 'F', '3106604599', '', 'CALLE 94 # 4A - 27       ', 'O +', 'NIEVES DEL CARMEN ORTEGA CABALLERO', '3005090513', 'COOMEVA', '2017-07-19 17:11:49'),
(1043682381, 'NUIP', '1', 'JUAN JOSE', 'CANOLES GONZALEZ', '', '', '0000-00-00', 'M', '3184412399', 'CARMEN_ANGELMIO@HOTMAIL.COM', 'CRA 2C N°94-80           ', 'O +', 'CARMINIA  VASQUEZ CAMARGO', '3017176516', 'COOMEVA', '2017-07-19 17:11:46'),
(1043682898, 'R.C.', '1', 'KRISTIAN ', 'SALAS POLO', '', '', '0000-00-00', 'M', '3005995317', '', 'CARRERA 6E # 75 B 20     ', 'A +', 'LAURA VANESSA POLO LEAL', '3016284133', 'SALUD TOTAL', '2017-07-19 17:11:45'),
(1043683090, 'R.C.', '2', 'MARK DIONY', 'MERCADO CHARRIS', '', '', '0000-00-00', 'M', '3104090899', '', 'CARRERA 4B#94-40         ', 'A -', 'LILIANA GREGORIA CHARRIS POLO', '3104090899', 'coomeva                       ', '2017-07-19 17:11:48'),
(1043683357, 'NUIP', '1', 'LAYERSON DE JESUS', 'GALVIS MEDINA', '', '', '0000-00-00', 'M', '3015490419', '', 'CRA 11A N 87-16          ', 'A +', 'DORIS ESTHER MEDINA GUZMAN', '3015490419', 'MUTUAL SER                    ', '2017-07-19 17:11:45'),
(1043683387, 'NUIP', '1', 'SHARON ORIANA', 'CORTES FONTALVO', '', '', '0000-00-00', 'F', '3015864280', 'shirlyfontalvo-03@hotmail.com', 'CLL 92 Nº 3-25           ', 'O +', 'SHIRLY ELENA FONTALVO RIOS', '3015864280', 'NUEVA EPS                     ', '2017-07-19 17:11:45'),
(1043686819, 'NUIP', 'TRANSICION', 'ALBA SANDRY', 'DE LEON ARROYO', '', '', '0000-00-00', 'F', '3017054440', 'lucia05071@hotmail.com', 'CRA 3A Nº 94-60          ', 'O +', 'EDUIN EDGAR DE LEON FORNARIS', '3017054440', 'COOMEVA', '2017-07-19 17:12:01'),
(1043687774, 'NUIP', 'TRANSICION', 'MARIANA ISABEL', 'VERA RUSSO', '', '', '0000-00-00', 'F', '3154145699', '', 'CRA 1E Nº 90-91          ', 'A +', 'REINALDO  VERA BARON', '3154145699', 'SALUD TOTAL', '2017-07-19 17:12:01'),
(1043688184, 'NUIP', 'TRANSICION', 'MATHIAS ', 'WILCHES OBANDO', '', '', '0000-00-00', 'M', '3166279826', '', 'CALLE 91 Nº 6A SUR - 59  ', 'O +', 'JENNYFER ESTHER OBANDO CUELLO', '3016403764', 'MUTUALSER                     ', '2017-07-19 17:12:02'),
(1043688195, 'NUIP', 'TRANSICION', 'TALIANA MARIA', 'RODRIGUEZ NIÑO', '', '', '0000-00-00', 'F', '3002022888', '', 'CRA 2 SUR Nº 91-04       ', 'O +', 'SANDRA MILENA NIÑO GUTIERREZ', '3002022888', '', '2017-07-19 17:12:01'),
(1043688928, 'NUIP', 'JARDIN', 'SEBASTIAN ANDRES', 'FELIPE MONTENEGRO', '', '', '0000-00-00', 'M', '3045438591', '', 'CRA 3C Nº 92-80          ', 'O +', 'JOSE GREGORIO FELIPE ARIZA', '3015104139', 'MUTUAL SER                    ', '2017-07-19 17:11:59'),
(1043689519, 'R.C.', 'JARDIN', 'VALENTINA ', 'JIMENEZ PADILLA', '', '', '0000-00-00', 'F', '3147534392', '', 'CARRERA 3A # 78 - 44     ', 'B +', 'MARIANELA  PADILLA MEDINA', '3147534392', 'MUTUALSER                     ', '2017-07-19 17:11:59'),
(1043689694, 'C.C.', 'JARDIN', 'DANIEL ', 'VERGARA DEL VALLE', '', '', '0000-00-00', 'M', '3017385699', '', 'CALLE 92 # 3 - 40        ', 'O +', 'HEVERS ANTONIO VERGARA VERGARA', '3017385699', 'SALUD TOTAL', '2017-07-19 17:12:00'),
(1043690622, 'NUIP', 'JARDIN', 'CAMILA ANDREA', 'ALTAMIRANDA PERTUZ', '', '', '0000-00-00', 'F', '3002725377', '', 'CRA 4A Nº 80 - 73        ', 'O +', 'AUDIS ASTRID PERTUZ QUIROZ', '3002725377', 'COOMEVA', '2017-07-19 17:12:00'),
(1043691341, 'NUIP', 'JARDIN', 'ALFONSO JOSÉ', 'PACHECO PEREZ', '', '', '0000-00-00', 'M', '3136335107', '', 'CARRERA 3 Nº 94 - 29     ', 'A +', 'ALFONSO  PACHECO ALCAZAR', '3114378424', 'SURA                          ', '2017-07-19 17:11:59'),
(1043692944, 'NUIP', 'PREJARDIN', 'DANIEL DAVID', 'QUINCHARA SARMIENTO', '', '', '0000-00-00', 'M', '3045812277', '', 'CRA 4 Nº 91-29           ', '', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', '', '2017-07-19 17:12:00'),
(1043693259, 'NUIP', 'PREJARDIN', 'ANA LUCIA', 'ORDOÑEZ SANDOVAL', '', '', '0000-00-00', 'F', '3004298442', '', 'CRA 3A N 92-17           ', 'O +', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', 'COOMEVA', '2017-07-19 17:12:00'),
(1043693489, 'NUIP', 'PREJARDIN', 'SHAYRA MARCELA', 'PEREZ DE LA ROSA', '', '', '0000-00-00', 'F', '3002969602', '', 'CRA 3 N 94-30            ', 'O +', 'CLAUDIA MARCELA DE LA ROSA GARCIA', '3002969602', 'SALUD TOTAL', '2017-07-19 17:12:00'),
(1043693797, 'T.I.', '7', 'SARA MICHELL', 'PALMA BARRIOS', '', '', '0000-00-00', 'F', '3005347609', 'georys_palma@hotmail.com', 'CRA 3C Nº 80-65          ', 'O +', 'GEORIS ANTONIO PALMA SERRANO', '3136830518', 'SALUD TOTAL', '2017-07-19 17:11:55'),
(1044212709, 'T.I.', '6', 'ALEJANDRO ISAAC', 'GOMEZ VARGAS', '', '', '0000-00-00', 'M', '3006133372', '', 'CALLE 96 N1E 225         ', 'O +', 'MARIELA  CONTRERAS GOMEZ', '3135007019', 'COOMEVA', '2017-07-19 17:11:54'),
(1044214576, 'T.I.', '5', 'KEYLEEN ARIANA', 'COBA LOPEZ', '', '', '0000-00-00', 'F', '3205160938', 'janethlopez352@gmail.com', 'CRA 3B Nº 88-73          ', 'O +', 'JANETH PATRICIA LOPEZ VENTA', '3205160938', 'SALUD TOTAL', '2017-07-19 17:11:52'),
(1044215202, 'T.I.', '5', 'ALEJANDRA MARIA', 'SERRANO MESA', '', '', '2039-08-02', 'F', '3043774170', '', 'CLL 94 Nº 4B-26          ', 'O +', 'KATTY SOFIA SERRANO MORENO', '3003511998', 'COOMEVA', '2017-07-19 17:11:53'),
(1044215962, 'R.C.', '5', 'YANDIEL ', 'MUNERA PEREZ', '', '', '0000-00-00', 'M', '3118034656', '', 'CALLE 4A Nº 61 27        ', 'A +', 'LUIS ALBERTO MUNERA LOPEZ', '3118034656', 'SURA                          ', '2017-07-19 17:11:53'),
(1044218711, 'R.C.', '3', 'BETSY PAOLA', 'MUNERA PEREZ', '', '', '0000-00-00', 'F', '3118034656', '', 'CALLE 4A Nº61 27         ', 'O +', 'LUIS ALBERTO MUNERA LOPEZ', '3118034656', 'SURA                          ', '2017-07-19 17:11:50'),
(1044219306, 'T.I.', '3', 'GIBSY SAYLI', 'ALTAMAR IGLESIAS', '', '', '2040-06-06', 'F', '3196488910', '', 'CRA 8SUR Nº 95-105       ', 'O +', 'VANESSA  IGLESIAS PERTUZ', '3196488910', 'CAFE SALUD                    ', '2017-07-19 17:11:50'),
(1044219981, 'R.C.', '2', 'ALAN DAVID', 'ROJAS VILLALOBO', '', '', '0000-00-00', 'M', '3002719177', 'villalobosbeatriz@hotmail.com', 'CALLE 98C # 6E - 85      ', 'O +', 'BEATRIZ  VILLALOBOS CARRASCAL', '3012599913', 'COOMEVA', '2017-07-19 17:11:49'),
(1044220692, 'NUIP', '1', 'JONATHAN ALEMAO', 'COLMENARES PARRA', '', '', '0000-00-00', 'M', '3103626224', '', 'CLL 90 Nº 2C-43          ', 'A +', 'GIANINA PAOLA PARRA DE LA HOZ', '3103626224', 'COOSALUD                      ', '2017-07-19 17:11:45'),
(1044222321, 'NUIP', 'TRANSICION', 'LUNA SHADID', 'BERTEL POLO', '', '', '2041-05-02', 'F', '3165254577', 'joseluisbertelpolo@hotmail.com', 'CRA 3A Nº 92-49          ', 'O +', 'SAMIRA ESTHER POLO MENDOZA', '3165254577', 'SALUD TOTAL', '2017-07-19 17:12:01'),
(1044222930, 'NUIP', 'JARDIN', 'KERHEEN SOFIA', 'COBA LOPEZ', '', '', '0000-00-00', 'F', '3205160938', 'janethlopez352@gmail.com', 'CRA 4B Nº 88-73          ', '', 'JANETH PATRICIA LOPEZ VENTA', '3205160938', '', '2017-07-19 17:11:59'),
(1044421082, 'T.I.', '7', 'OSCAR GUILLERMO', 'MANCERA TEHERAN', '', '', '0000-00-00', 'M', '3045704304', 'samy-temu@hotmail.com', 'CRA 18 Nº 47B-27         ', '', 'SANDRA MILENA THERAN MUÑOZ', '3045704304', '', '2017-07-19 17:11:55'),
(1044604111, 'T.I.', '6', 'SHARICK ', 'PAJARO MORENO', '', '', '2038-09-01', 'F', '3205167498', '', 'CALLE 88 # 2C - 15       ', 'O -', 'LUISA ROSA TOVAR ROMERO', '3205167498', 'SALUD TOTAL', '2017-07-19 17:11:54'),
(1044604638, 'T.I.', '7', 'JOSE YAMID', 'ARMESTO ROMERO', '', '', '0000-00-00', 'M', '3126237765', '', 'CRA 1 Nº 6A SUR-103      ', 'O +', 'MARIA JOSE ROMERO ACOSTA', '3126237765', 'COOMEVA', '2017-07-19 17:11:55'),
(1044606141, 'T.I.', '7', 'JORDY JOSÈ', 'GALVIS MEDINA', '', '', '0000-00-00', 'M', '3012451827', '', 'CARRERA 11A N 87-16      ', 'O +', 'DORIS ESTHER MEDINA GUZMAN', '3015490419', 'COOSALUD                      ', '2017-07-19 17:11:55'),
(1044606315, 'T.I.', '7', 'CALEB GERARDO', 'VERA AMAYA', '', '', '0000-00-00', 'M', '3005538056', '', 'CALLE 90 # 6 B - 72      ', 'A +', 'YOLANDA  AMAYA JAIME', '3005174667', 'COOMEVA', '2017-07-19 17:11:56'),
(1044606765, 'T.I.', '6', 'RUTH MARIA', 'RIVERA RIQUEME', '', '', '0000-00-00', 'F', '3008149060', 'geovanirivera12@hotmail.com', 'CRA 4C Nº 92-40          ', 'O +', 'GIOVANNI FIDEL RIVERA PARRA', '3015084603', '', '2017-07-19 17:11:54'),
(1044607154, 'T.I.', '8', 'LAUREN ISABEL ', 'DE LA CRUZ VALDES', '', '', '0000-00-00', 'F', '3003835958', 'laurents13122004@hotmail.com', 'CLL 80 Nº 2C-50          ', 'O +', 'LEIVYS ISABEL VALDES CASIANI', '3003835958', 'CAPRECON                      ', '2017-07-19 17:11:56'),
(1044608244, 'T.I.', '6', 'JAVIER REYNALDO', 'VERA RUSSO', '', '', '0000-00-00', 'M', '3154145699', '', 'CARRERA 1 E # 90 - 91    ', 'O +', 'REINALDO  VERA BARON', '3154145699', 'SALUD TOTAL', '2017-07-19 17:11:54'),
(1044608934, 'T.I.', '7', 'MAYSSA ', 'GALEANO ZAPATA', '', '', '0000-00-00', 'F', '3045672390', '', 'CARRERA 3 SUR # 89-27    ', 'O +', 'WILLIAM ANTONIO GALEANO SANCHEZ', '3012575563', 'SANITAS                       ', '2017-07-19 17:11:55'),
(1044613075, 'T.I.', '6', 'KAREN ANDREA', 'MORALES HORTA', '', '', '0000-00-00', 'F', '3145664717', 'monicahortan@hotmail.com', 'CALLE 92 NO. 3A-05       ', 'O +', 'MONICA ESTHER HORTA NARANJO', '3145664717', 'COOMEVA', '2017-07-19 17:11:54'),
(1044614415, 'T.I.', '6', 'LAURA VANESSA', 'SANTIZ CORONELL', '', '', '0000-00-00', 'F', '3163367392', 'alvarosantiz@canguro.com.co', 'CRA 3 N° 88-99           ', 'O +', 'ALVARO ENRIQUE SANTIZ CUDRIZ', '3163367392', 'COOMEVA', '2017-07-19 17:11:54'),
(1044615857, 'T.I.', '6', 'JOHAN ESTIBEN', 'WANDURRAGA VELASQUEZ', '', '', '0000-00-00', 'M', '3138551775', '', 'CRA 6R Nº 97-119         ', 'A +', 'ROSA MARIA VELASQUEZ SANCHEZ', '3135264392', 'salud riesgo                  ', '2017-07-19 17:11:54'),
(1044618176, 'T.I.', '6', 'CESAR ISAAC', 'ACOSTA GARCIA', '', '', '0000-00-00', 'M', '3126387868', '', 'CARRERA 3B Nº 88 - 113   ', 'A +', 'CARMEN ROSA GARCIA BARRIENTOS', '3162127505', 'COOMEVA', '2017-07-19 17:11:53'),
(1044621072, 'T.I.', '4', 'OMAR SEBASTIAN', 'OSPINO BLANCO', '', '', '2039-09-08', 'M', '3014530303', '', 'CALLE 94 # 6 A - 27      ', 'B +', 'DIANUBIS YARIS BLANCO GARCIA', '3242527', 'SALUD TOTAL', '2017-07-19 17:11:52'),
(1044621300, 'T.I.', '5', 'DAINER ANTONIO', 'QUINTANA PALMA', '', '', '2039-12-02', 'M', '3017924606', '', 'CLL 73F Nº 5F-72         ', 'O +', 'SELENA MARGARITA PALMA OROZCO', '3017924606', 'COOMEVA', '2017-07-19 17:11:53'),
(1044623397, 'NUIP', '5', 'ADRIANA LUCIA', 'DE LEON ARROYO', '', '', '0000-00-00', 'F', '3043653650', '', 'CRA 3A Nº 94-60          ', 'A +', 'EDUIN EDGAR DE LEON FORNARIS', '3017054440', 'COOMEVA', '2017-07-19 17:11:52'),
(1044623621, 'NUIP', '4', 'CRISTIAN MIGUEL', 'MORALES HORTA', '', '', '0000-00-00', 'M', '3469418', '', 'CLLE 92 N 3A - 05        ', 'A +', 'MONICA ESTHER HORTA NARANJO', '3145664717', 'COOMEVA', '2017-07-19 17:11:51'),
(1044624130, 'T.I.', '4', 'HAROLD LEONARD', 'GUTIERREZ QUESADA', '', '', '0000-00-00', 'M', '3043834081', 'adri0722@hotmail.com', 'CRA 3B Nº 90-50          ', 'O +', 'ADRIANA ISABEL QUESADA JULIO', '3043834081', 'SALUD COOP                    ', '2017-07-19 17:11:51'),
(1044624240, 'T.I.', '5', 'RONALD DANIEL', 'MARTINEZ CANTILLO', '', '', '0000-00-00', 'M', '3177548662', '', 'CARRERA 6 # 95 - 12      ', 'O +', 'RONAL DE JESUS MARTINEZ VILLADIEGO', '3177548662', 'EPS SANITA                    ', '2017-07-19 17:11:53'),
(1044624454, 'NUIP', '4', 'HANNAH ', 'ARIZA ESTEVEZ', '', '', '0000-00-00', 'F', '3146335474', 'dj27h-h@hotmail.com', 'CRA 5 Nº 92 05           ', 'O +', 'DURLEY JOHANA ESTEVEZ RUBIO', '3146335474', 'SALUD TOTAL', '2017-07-19 17:11:51'),
(1044625528, 'T.I.', '4', 'DANNY DE JESUS', 'VARGAS POLANCO', '', '', '0000-00-00', 'M', '3057915259', 'xxx@hotmail.com', 'CRA 5B N° 89-39          ', 'AB +', 'LUZCARY  POLANCO PAYARES', '3002559413', 'COOMEVA', '2017-07-19 17:11:51'),
(1044626668, 'T.I.', '4', 'MATEO ANDRES', 'PERTUZ ROBLES', '', '', '0000-00-00', 'M', '3014466797', 'nicolas.pertuz@hotmail.com', 'CRA 8 SUR N° 97-40       ', 'O +', 'HEIMY BETH ROBLES PEREZ', '3014466797', 'COOMEVA', '2017-07-19 17:11:51'),
(1044627339, 'R.C.', '4', 'NICOLL ESTHER', 'BONOLI ALCENDRA', '', '', '0000-00-00', 'F', '3015740062', '', 'CALLE 90 # 4B-14         ', 'O +', 'ISAURA VILELMA ALCENDRA MESA', '3015740062', 'COOMEVA', '2017-07-19 17:11:51'),
(1044628517, 'NUIP', '4', 'KAYWIL HENRY', 'PARRA HOYOS', '', '', '0000-00-00', 'M', '3135539996', 'kaywilhenry@hotmail.com', 'CRA 6G Nº 97-68          ', 'O +', 'KATY LUZ HOYOS GONZALEZ', '3168549727', 'COOMEVA', '2017-07-19 17:11:52'),
(1044630646, 'R.C.', '3', 'TALIANA MICHEL', 'SANTIAGO PAJARO', '', '', '0000-00-00', 'F', '3162126014', '', 'CALLE 80#3B-05           ', 'O +', 'MARNIES DOLORES PAJARO REYES', '3162126014', 'salud total                   ', '2017-07-19 17:11:50'),
(1044630764, 'R.C.', '3', 'DINA LUZ', 'JIMENEZ MAESTRE', '', '', '0000-00-00', 'F', '3205499371', 'JOMADI1@HOTMAIL.ES', 'CALLE 89B Nº 6E - 20     ', 'A +', 'EMIL JOHANA MAESTRE ACUÑA', '3205499371', 'SALUD TOTAL', '2017-07-19 17:11:49'),
(1044631140, 'NUIP', '3', 'LAURA MELISA', 'CARVAJAL COLMENARES', '', '', '0000-00-00', 'F', '3147810167', 'lucia05071@hotmail.com', 'CLL 90 Nº 6A-32          ', 'A +', 'JULIETH PAOLA ARROYO CARVAJAL', '3005776224', 'COOMEVA', '2017-07-19 17:11:50'),
(1044631740, 'NUIP', '3', 'VALERIA ESTER', 'CANTILLO HURTADO', '', '', '0000-00-00', 'F', '3006662976', '', 'CRA 4 B Nº 88-64         ', 'B +', 'ALEXIS  HURTADO RUIZ', '3004677168', '', '2017-07-19 17:11:49'),
(1044633607, 'NUIP', '3', 'MATIAS DANIEL', 'GUTIERREZ CABALLERO', '', '', '0000-00-00', 'M', '3016816013', 'mayiasesorcomercial@hotmail.co', 'CLL 91 Nº 6-137          ', 'B +', 'MARYOLIS MARIA CABALLERO AREVALO', '3016816013', 'COOMEVA', '2017-07-19 17:11:50'),
(1044633667, 'NUIP', '3', 'CARLOS EDUARDO', 'CORZO BUELVAS', '', '', '0000-00-00', 'M', '3225115851', 'xxx@hotmail.com', 'CRA 6E N° 99C-08         ', 'O +', 'MERCEDES ELENA BUELVAS BUELVAS', '3225115851', 'SOLSALUD                      ', '2017-07-19 17:11:49'),
(1044637611, 'NUIP', '1', 'BREINER ANDRES', 'GUTIERREZ VILLAREAL', '', '', '2040-12-06', 'M', '3106008665', '', 'CALLE 99 Nº 6C - 81      ', 'A +', 'MARIA LEONOR VILLAREAL CALA', '3126052548', 'SALUD TOTAL', '2017-07-19 17:11:45'),
(1044640006, 'NUIP', '1', 'LUIS EDUARDO', 'CORZO BUELVAS', '', '', '0000-00-00', 'M', '3116993335', 'xxx@hotmail.com', 'CRA 6E Nº 99C-08         ', 'O +', 'MERCEDES ELENA BUELVAS BUELVAS', '3225115851', 'SALUD TOTAL', '2017-07-19 17:11:45'),
(1044641887, 'NUIP', '1', 'JOEL ', 'MERCADO GALOFRE', '', '', '0000-00-00', 'M', '', 'rubygalofre@hotmail.com', 'CRA 2 SUR Nº 80-28       ', 'O +', 'ARMANDO ENRIQUE GALOFRE PEREZ', '3116792392', 'XXX                           ', '2017-07-19 17:11:45'),
(1044642380, 'R.C.', '1', 'JOYCE FERNANDA', 'SALAZAR PUA', '', '', '0000-00-00', 'F', '3016687899', '', 'CARRERA 4 B # 92 - 19    ', 'B +', 'DIVINA LUZ PUA VILLA', '3016687899', 'COOMEVA', '2017-07-19 17:11:45'),
(1044645348, 'R.C.', 'TRANSICION', 'CAMILO EDUARDO', 'VELILLA HERRERA', '', '', '0000-00-00', 'M', '3186234206', 'MILISITA1986@HOTMAIL.COM', 'CALLE 88 N 2D 14         ', 'O +', 'MILAGROS  HERRERA MOJICA', '3168626291', 'COOMEVA', '2017-07-19 17:12:01'),
(1044648716, 'NUIP', 'JARDIN', 'ANA SHARAY', 'ACOSTA GARCIA', '', '', '0000-00-00', 'F', '3126387868', '', 'CARRERA 3 B Nº 88 - 113  ', 'O +', 'CARMEN ROSA GARCIA BARRIENTOS', '3162127505', 'COOMEVA', '2017-07-19 17:11:59'),
(1044652222, 'NUIP', 'PREJARDIN', 'KENNETH CARLOS', 'RODRIGUEZ RODRIGUEZ', '', '', '0000-00-00', 'M', '3008809434', 'LUZ.RODRIGUEZ01@HOTMAIL.COM', 'CRA 3C Nº94 70           ', '', 'LUZ DARY RODRIGUEZ DIAZ', '3008809434', '', '2017-07-19 17:12:00'),
(1045702697, 'NUIP', '2', 'SANTIAGO ', 'BETANCOURT TAPIA', '', '', '2040-08-07', 'M', '3013757550', 'andre0214@hotmail.com', 'CLL 92 Nº 1E-11          ', 'A +', 'ANDREA PAOLA TAPIA SANTOS', '3004421741', 'COOMEVA', '2017-07-19 17:11:48'),
(1045704001, 'R.C.', '3', 'MELANIE ISABEL', 'SAUCEDO ROMERO', '', '', '2040-07-05', 'F', '3135659603', 'yelisao3101@hotmail.com', 'CLL 94 Nº 5-25           ', 'O +', 'MONICA ISABEL GARCIA VILLARREAL', '3012755060', 'CAJACOPI                      ', '2017-07-19 17:11:50'),
(1045718052, 'NUIP', 'TRANSICION', 'MORIS ANDRE', 'CARABALLO LOZANO', '', '', '0000-00-00', 'M', '3114294718', '', 'CALLE 98 B Nº 6 B - 16   ', 'O +', 'MONICA PATRICIA LOZANO LOPEZ', '3205085653', 'CLINICA D ELA POLICIA         ', '2017-07-19 17:12:01'),
(1045729968, 'NUIP', '1', 'KIMBERLY ESTHER', 'QUIROGA BASTIDAS', '', '', '0000-00-00', 'F', '3016336322', '', 'CARRERA 4 A Nº 91 - 61   ', 'O +', 'MERY ESTHER OSPINO GARCIA', '3004559434', 'BARRIOS UNIDOS DE QUIBDO      ', '2017-07-19 17:11:45'),
(1046694543, 'T.I.', '7', 'SEBASTIAN ', 'RICARDO GUTIERREZ', '', '', '0000-00-00', 'M', '3108308304', 'yunismargarita14@hotmail.com', 'CLL 94 Nº 2D-25          ', '', 'YUNIS MARGARITA GUTIERREZ MORALES', '3108308304', '', '2017-07-19 17:11:55'),
(1046702269, 'NUIP', '4', 'YUSBLEIDIS SAYOHAR', 'ARIAS ALVAREZ', '', '', '0000-00-00', 'F', '3016740199', '', 'CRA 4 Nº 92-23           ', 'O +', 'MARIA NADREA ALVAREZ HERRERA', '3104567920', '                              ', '2017-07-19 17:11:51'),
(1046703986, 'T.I.', '7', 'CHELSEA FRANCINE', 'PADILLA VEGA', '', '', '0000-00-00', 'F', '3045505023', '', 'CRA 1F Nº 91-03          ', 'O +', 'WENDI PAOLA VEGA ROLONG', '3014792186', 'SANIDAD NAVAL                 ', '2017-07-19 17:11:55'),
(1046704769, 'NUIP', '3', 'TANYA ALEJANDRA', 'CANOLES OLASCOAGA', '', '', '0000-00-00', 'F', '3012614963', '', 'CRA 3A Nº91-42           ', 'B +', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', 'COOMEVA', '2017-07-19 17:11:49'),
(1046705247, 'NUIP', '2', 'GABRIELA DE LOS ANGELES', 'MARTINEZ NAVARRO', '', '', '2040-06-06', 'F', '3107144094', '', 'CLL 90 Nº 2B-43          ', 'O +', 'ESTELA  NAVARRO CACERES', '3107144094', 'COOMEVA', '2017-07-19 17:11:48'),
(1046705478, 'NUIP', '1', 'YERLIS LUCIA', 'GONZALEZ SEHUANES', '', '', '0000-00-00', 'F', '3126723966', '', 'CRA 3A Nº 91-42          ', 'O +', 'ENERGINA  SEHUANES RODRIGUEZ', '3126723966', 'COOMEVA', '2017-07-19 17:11:44'),
(1046706227, 'R.C.', '2', 'LAURA SOFIA', 'MIRANDA TORRENEGRA', '', '', '0000-00-00', 'F', '3104396464', '', 'CARRERA 3C # 91 - 76     ', 'O +', 'SHIRLEY DEL CARMEN TORRENEGRA OLIVERO', '3045944328', 'COOMEVA', '2017-07-19 17:11:48'),
(1046706249, 'R.C.', '2', 'ALAN DANIEL', 'ESTEVEZ RUBIO', '', '', '0000-00-00', 'M', '3012631097', '', 'CRA 5 C 91-65            ', 'O +', 'LIZETH YESENIA ESTEVEZ RUBIO', '3012631097', 'salud total                   ', '2017-07-19 17:11:48'),
(1046706479, 'NUIP', '2', 'LUCAS TONY', 'GONZALEZ SEHUANES', '', '', '0000-00-00', 'M', '3126723966', '', 'CRA 3A Nº 91-42          ', 'O +', 'ENERGINA  SEHUANES RODRIGUEZ', '3126723966', 'COOMEVA', '2017-07-19 17:11:48'),
(1046706929, 'NUIP', '2', 'MARIA JOSE', 'LONDOÑO RAMOS', '', '', '0000-00-00', 'F', '3176868032', 'cenithramos@hotmail.com', 'CLL 54 Nº 3E-76          ', 'A +', 'CENITH ESTER RAMOS POLO', '3012606493', 'COOMEVA', '2017-07-19 17:11:49'),
(1046709011, 'NUIP', 'TRANSICION', 'VALENTINA ', 'OSPINA DIAZ', '', '', '0000-00-00', 'F', '3002183588', '', 'CARRERA 1 Nº 90 - 43     ', 'B +', 'LUIS EDUARDO OSPINA SANCHEZ', '3002183588', 'MUTUALSER                     ', '2017-07-19 17:12:01'),
(1046709137, 'NUIP', '1', 'LINA GABRIELA', 'PARDO PEÑA', '', '', '0000-00-00', 'F', '3145152716', 'jaque-laulina@hotmail.com', 'CLL 81 Nº 20-35          ', 'O +', 'JAQUELINE  PEÑA ACUÑA', '3114051206', 'salud total                   ', '2017-07-19 17:11:46'),
(1046709741, 'R.C.', 'TRANSICION', 'SAMUEL DAVID', 'MONTENEGRO OJEDA', '', '', '0000-00-00', 'M', '3004138029', 'ivanmadridojeda@outlook.es', 'CALLE 96 # 3 C - 06      ', 'B +', 'CECILIA ELENA OJEDA ELGUEDO', '3004138029', 'SALUD COOP                    ', '2017-07-19 17:12:01'),
(1046711418, 'R.C.', 'TRANSICION', 'ASHLEY MEILLY', 'GALEANO ZAPATA', '', '', '0000-00-00', 'F', '3045672390', '', 'CARRERA 3 SUR # 89-27    ', 'O +', 'WILLIAM ANTONIO GALEANO SANCHEZ', '3012575563', 'SANITAS Y COLSANITAS          ', '2017-07-19 17:12:01'),
(1046712145, 'NUIP', 'TRANSICION', 'YUSLEIDYS MILAGROS', 'ARIAS ALVAREZ', '', '', '2041-00-03', 'F', '3016740199', '', 'CRA 4 Nº 92-23           ', 'O +', 'MARIA NADREA ALVAREZ HERRERA', '3104567920', '                              ', '2017-07-19 17:12:01'),
(1046713513, 'NUIP', 'PREJARDIN', 'JESUS DAVID', 'NAVAS VILLAREAL', '', '', '0000-00-00', 'M', '3005479622', '', 'CALLE 90 Nº 3C 45        ', 'O +', 'YESSICA DEL CARMEN VILLARREAL DE LA HOZ', '3005479622', 'SALUD TOTAL', '2017-07-19 17:12:00'),
(1046714278, 'NUIP', 'JARDIN', 'JUAN DAVID', 'RUA MARTINEZ', '', '', '0000-00-00', 'M', '3016364362', 'monik240909@hotmail.com', 'CRA 6A SUR Nº 90-56      ', 'O +', 'MONICA PATRICIA MARTINEZ GUERRERO', '3016364362', 'SALUD TOTAL', '2017-07-19 17:12:00'),
(1046714712, 'NUIP', 'JARDIN', 'JAIRO DE JESÚS', 'DE ORO PERNET', '', '', '0000-00-00', 'M', '3017539862', '', 'CARRERA 2 Nº 62 - 15     ', 'O +', 'SUGEY  PERNETT TORRES', '3017539862', 'COOMEVA', '2017-07-19 17:11:59'),
(1046715549, 'NUIP', 'JARDIN', 'SOFIA MARIANA', 'GUERRERO HERNANDEZ', '', '', '0000-00-00', 'F', '3113890956', '', 'CALLE 96 A Nº 5 SUR - 04 ', 'A +', 'ANGELA LORENA HERNANDEZ MAZA', '3113890956', 'SURAMERICANA                  ', '2017-07-19 17:11:59'),
(1046715858, 'NUIP', 'JARDIN', 'KEREN DANIELA', 'GONZALEZ HERRERA', '', '', '0000-00-00', 'F', '3183727968', '', 'CALLE 88 Nº 2D - 04      ', 'O +', 'KAREN PAOLA HERRERA MOJICA', '3183460973', 'SURA                          ', '2017-07-19 17:11:59'),
(1046716198, 'NUIP', 'JARDIN', 'MARIA DANIELA', 'GARCIA LOZANO', '', '', '0000-00-00', 'F', '3007732373', '', 'CARRERA 3 Nº 74 - 21     ', 'B +', 'LEIDYS  LOZANO CAMPOS', '3007732373', 'SURA                          ', '2017-07-19 17:11:59'),
(1046716372, 'NUIP', 'JARDIN', 'SOFIA ANDREA', 'BETANCOURT TAPIA', '', '', '0000-00-00', 'F', '3013757550', 'andre0214@hotmail.com', 'CLL 92 Nº 1E-11          ', 'A +', 'ANDREA PAOLA TAPIA SANTOS', '3004421741', '', '2017-07-19 17:11:59'),
(1046717470, 'NUIP', 'PREJARDIN', 'VALERIA ', 'MENDOZA CANTILLO', '', '', '0000-00-00', 'F', '3012616261', 'DANIELMENDOZA0786@GMAIL.COM', 'CLL 80 Nº 1B-02          ', 'O +', 'DANIEL JESUS MENDOZA MOGOLLON', '3012616261', 'COOMEVA', '2017-07-19 17:12:00'),
(1046719001, 'NUIP', 'PREJARDIN', 'SANTIAGO JOSE', 'DE AGUAS GUTIERREZ', '', '', '0000-00-00', 'M', '3004633460', '', 'CALLE 90 N 3A - 33       ', 'O +', 'MARIA DE LOS ANGELES GUTIERREZ PINO', '3004633460', 'SURA                          ', '2017-07-19 17:12:00'),
(1046719120, 'NUIP', 'PREJARDIN', 'KEILLYS GUADALUPE', 'CERMEÑO BARRIOS', '', '', '0000-00-00', 'F', '3015495829', '', 'CRA 1F N 90 84           ', 'A +', 'KELLY JOHANA BARRIOS SEPULVEDA', '3005878964', 'COOSALUD                      ', '2017-07-19 17:12:00'),
(1046719431, 'NUIP', 'PREJARDIN', 'DARIANA ', 'RAVE RUIZ', '', '', '0000-00-00', 'F', '3007225925', 'yamilis200@hotmail.com', 'CLL 90 Nº 6B-09          ', 'O +', 'YAMILIS MARIA RUIZ ALVAREZ', '3003884083', 'XXX                           ', '2017-07-19 17:12:00'),
(1046719984, 'NUIP', 'PREJARDIN', 'LUIS EDUARDO', 'VELILLA HERRERA', '', '', '0000-00-00', 'M', '3186234206', 'milisita1986@hotmail.com', 'CLL 88 Nº 2D-04          ', 'O +', 'MILAGROS  HERRERA MOJICA', '3168626291', 'COOMEVA', '2017-07-19 17:12:01'),
(1046720175, 'NUIP', 'PREJARDIN', 'THAEL PAOLA', 'MARTINEZ OLIVERA', '', '', '0000-00-00', 'F', '3168692129', 'leja_1970@yahoo.com', 'CRA 4 SUR Nº 96-35       ', '', 'MARIA VICTORIA OLIVERA JIMENEZ', '3233070836', '', '2017-07-19 17:12:00'),
(1047035864, 'T.I.', '8', 'STEFANY ', 'CANO VASCO', '', '', '2038-00-00', 'F', '3216846813', '', 'CRA 3C N° 88-98          ', 'B+', 'LILIANA LUCIA VASCO VASQUEZ', '3216846813', 'CAJACOPI                      ', '2017-07-19 17:11:57'),
(1047040302, 'T.I.', '5', 'JESUS MANUEL', 'GARCIA DE LA HOZ', '', '', '0000-00-00', 'M', '3135251810', 'jesusmanuel2006@hotmail.com', 'CARRERA 2A#73D-27        ', 'O +', 'MARIBEL DEL CARMEN DE LA HOZ FERNANDEZ', '3215100437', 'salud total                   ', '2017-07-19 17:11:52'),
(1047041061, 'T.I.', '5', 'CAMILA ALEXANDRA', 'BANQUEZ PASTRANA', '', '', '0000-00-00', 'F', '3005086004', '', 'CALLE 88 N 2B - 14       ', 'O +', 'CANDY PAOLA PASTRANA OYUELA', '3005086004', 'CLINICA DE LA POLICIA         ', '2017-07-19 17:11:52'),
(1047042181, 'NUIP', '5', 'KAYDI LORENA', 'DUNCAN MOLINA', '', '', '0000-00-00', 'F', '3005632885', 'kary-2009@live.com', 'CLL 72 N° 2C-15          ', 'O +', 'KARINE DE JESUS MOLINA BREY', '3015965384', 'SALUD COOP                    ', '2017-07-19 17:11:52'),
(1047043235, 'NUIP', '5', 'VALERIE DE JESUS', 'CASTRO MOLINA', '', '', '0000-00-00', 'F', '3007332552', 'carmen_2010@hotmail.es', 'CLL 72 Nº 2C-15          ', 'O +', 'CARMEN JOSEFINA MOLINA BREY', '3005632885', 'SALUD TOTAL', '2017-07-19 17:11:52'),
(1047043564, 'T.I.', '5', 'SAID HABIB', 'COLEY ROMERO', '', '', '0000-00-00', 'M', '3116739112', 'saidcoley@hotmail.com', 'CRA 5B Nº 92-31          ', 'A +', 'SANDRA MILENA ROMERO LICERO', '3116739112', 'SANIDAD DE LA POLICIA         ', '2017-07-19 17:11:52'),
(1047043597, 'T.I.', '5', 'JESÚS MANUEL', 'PINTO ARTETA', '', '', '0000-00-00', 'M', '3106374937', '', 'CARRERA 3B # 92 -90      ', 'A +', 'YURIS ELENA ARTETA GONZALEZ', '3106374937', 'SALUD COOP                    ', '2017-07-19 17:11:53'),
(1047045938, 'NUIP', '2', 'LUIS ESTEBAN', 'ROYERO ROMERO', '', '', '0000-00-00', 'M', '3016292400', '', 'CRA 6J Nº 91-56          ', 'A +', 'SILVIA ELENA COHEN CERPA', '3016292400', '', '2017-07-19 17:11:49'),
(1047046313, 'NUIP', '3', 'YHERSON ', 'RICARDO GUTIERREZ', '', '', '0000-00-00', 'M', '3108308304', '', 'CLL 94 N° 2D - 25        ', '', 'YUNIS MARGARITA GUTIERREZ MORALES', '3108308304', '', '2017-07-19 17:11:50'),
(1047049533, 'R.C.', '1', 'LUCAS PAUL', 'MIRANDA AGUILAR', '', '', '0000-00-00', 'M', '3186162998', '', 'CARRERA 4A # 91 - 50     ', 'O +', 'KELLY ESTHER GORDON DE RUIZ', '3242915', 'SALUDCOOP                     ', '2017-07-19 17:11:45'),
(1047050866, 'R.C.', 'JARDIN', 'JUAN DIEGO', 'CAMARGO DE CARO', '', '', '0000-00-00', 'M', '3013622569', '', 'CARRERA 3B # 90 - 60 APT ', 'AB +', 'RUBY ESTHER DE CARO GUTIERREZ', '3013622569', 'CAFESALUD                     ', '2017-07-19 17:11:59'),
(1047054589, 'NUIP', 'PREJARDIN', 'HILLARY ', 'RAVE DE AVILA', '', '', '0000-00-00', 'F', '3145199875', 'dayadadeavilab@hotmail.com', 'CRA 4A Nº 94-90          ', 'B -', 'DAYANA  DE AVILA BAÑOS', '3145199875', 'SALUD VIDA                    ', '2017-07-19 17:12:00'),
(1047225095, 'T.I.', '4', 'JOSÉ ANGEL', 'MAESTRE POLANCO', '', '', '0000-00-00', 'M', '3002904068', '', 'CARRERA 4 C # 90-43      ', 'A +', 'MIREDI DEL CARMEN POLANCO PALLARES', '3046238662', 'COOMEVA', '2017-07-19 17:11:51'),
(1047226053, 'R.C.', '4', 'MANUEL ALEJANDRO', 'MEJIA DIAZ', '', '', '0000-00-00', 'M', '3042468788', '', 'CALLE 94 # 2 C - 25      ', 'O +', 'YURIS PAOLA DIAZ ROMERO', '3145681636', 'CLINICA DE LA POLICIA         ', '2017-07-19 17:11:51'),
(1047230665, 'R.C.', '1', 'BREINER DANIEL', 'TORRES ALVAREZ', '', '', '0000-00-00', 'M', '3013230294', '', 'CALLE 92 # 4B - 06       ', 'A -', 'RODRIGO DE JESÚS TORRES TORRES', '3107280334', 'SALUD TOTAL', '2017-07-19 17:11:45'),
(1048064060, 'T.I.', '8', 'JOSE ALEJANDRO', 'MARQUEZ CARDENAS', '', '', '0000-00-00', 'M', '3113232257', '', 'CARRERA 6C # 99 C - 57   ', 'A +', 'NEFER ENRIQUE MARQUEZ MARQUEZ', '3205413247', 'SALUD TOTAL', '2017-07-19 17:11:57'),
(1048065942, 'T.I.', '7', 'KEISSY LILIANA', 'CERMEÑO BARRIOS', '', '', '0000-00-00', 'F', '3005878964', '', 'CRA 1F Nº 90-84          ', 'A +', 'KELLY JOHANA BARRIOS SEPULVEDA', '3005878964', 'COOSALUD                      ', '2017-07-19 17:11:55'),
(1048065980, 'T.I.', '7', 'ROBERTO JOSE', 'RODRIGUEZ BARROS', '', '', '0000-00-00', 'M', '3216760716', 'tefiro914@hotmail.com', 'CALLE 92 # 2C - 35       ', 'O +', 'CLAUDIA PATRICIA BARROS URRUTIA', '3216760716', 'CAFESALUD                     ', '2017-07-19 17:11:55'),
(1048069587, 'T.I.', '5', 'ALEJANDRA ', 'PEÑA ROA', '', '', '2039-11-09', 'F', '3114109098', '', 'CARRERA 6 N # 101 - 91   ', 'O +', 'YULAN NARED ROA SALAZAR', '3145498348', 'NUEVA EPS                     ', '2017-07-19 17:11:53'),
(1048069898, 'T.I.', '5', 'DANIEL ESTEBAN', 'GOMEZ CHAMORRO', '', '', '0000-00-00', 'M', '3106589905', 'catherine.chamorro@hotmail.com', 'CRA 4B N° 88-117         ', 'O +', 'CATHERINE  CHAMORRO MORON', '3106589905', 'COOMEVA', '2017-07-19 17:11:52'),
(1048070411, 'R.C.', '4', 'JUAN CAMILO', 'ALMANZA GAMARRA', '', '', '0000-00-00', 'M', '3216326315', '', 'TRANSVERSAL 2 # 80 - 105 ', 'B +', 'CANDELARIA DEL CARMEN GAMARRA ALARCON', '3217455628', 'CLINICA DE LA POLICIA         ', '2017-07-19 17:11:50'),
(1048070490, 'NUIP', '4', 'RAFAEL ISAAC', 'RAMIREZ SEPULVEDA', '', '', '0000-00-00', 'M', '3023314495', 'estrellamarina0508@hotmail.com', 'CRA 6 SUR Nº 99C-96      ', 'O +', 'CINDY JOHANA SEPULVEDA OTERO', '3012516402', 'COOMEVA', '2017-07-19 17:11:51'),
(1048071411, 'R.C.', '3', 'VICTOR MODESTO', 'VIAÑA AYOS', '', '', '0000-00-00', 'M', '', '', 'CARRERA 6 # 72-208       ', 'B +', 'ESTELA MARINA SALCEDO GONZALEZ', '3107413132', 'SALUD COOP                    ', '2017-07-19 17:11:50'),
(1048073305, 'NUIP', '3', 'DAILYN MISHELL', 'GUTIERREZ BONETT', '', '', '0000-00-00', 'F', '3013419538', '', 'CLL 92 Nº 2C-36          ', 'O +', 'MARTINA  BONETT PATERNINA', '3016179789', 'MUTUAL SER                    ', '2017-07-19 17:11:49'),
(1048074147, 'T.I.', '3', 'SHARON NICOLL', 'MONSALVE PEREZ', '', '', '2040-11-04', 'F', '3007439427', '', 'CRA 3A Nº 78-89          ', 'A+', 'YARILIS PAOLA PEREZ PEREZ', '3012667991', '', '2017-07-19 17:11:50'),
(1048074286, 'NUIP', '2', 'MATIAS DAVID', 'MARTINEZ ROYERO', '', '', '0000-00-00', 'M', '3012163767', 'yuri3royero@hotmail.com', 'CRA 6J Nº 91-56          ', 'O +', 'YURI VANESSA ROYERO COHEN', '3012163767', 'IPS                           ', '2017-07-19 17:11:48'),
(1048074558, 'NUIP', '2', 'MARIA VICTORIA', 'MEDINA VILLARREAL', '', '', '0000-00-00', 'F', '3005479622', 'villarrealdelahoz22@hotmail.co', 'CALLE 90 3C-45           ', 'B +', 'YESSICA DEL CARMEN VILLARREAL DE LA HOZ', '3005479622', 'SALUD TOTAL', '2017-07-19 17:11:49'),
(1048075051, 'NUIP', '1', 'CARLOS ANDRES', 'MARTINEZ MURILLO', '', '', '0000-00-00', 'M', '3126730704', '', 'CALLE 99D NO. 6E-83 APTO ', 'O -', 'BETTY  BERMUDES DE OLASCOAGA', '3012614963', 'CAFESALUD                     ', '2017-07-19 17:11:45'),
(1048075080, 'NUIP', '1', 'VALERY DANIELA', 'GARCÍA SEPULVEDA', '', '', '0000-00-00', 'F', '3012516402', 'e.fernandogarcia@hotmail.com', 'CRA 6SUR N° 99C-96 20 DE ', 'B +', 'CINDY JOHANA SEPULVEDA OTERO', '3012516402', 'COOMEVA', '2017-07-19 17:11:44'),
(1048077842, 'NUIP', 'PREJARDIN', 'SAHIR DAVID', 'DE LA ROSA ROLDAN', '', '', '2041-06-01', 'M', '3004476440', '', 'CALLE 81 Nº 4A-39        ', 'O +', 'MARY JULY ROLDAN RAMOS', '3004476440', 'SURA                          ', '2017-07-19 17:12:00'),
(1048077856, 'R.C.', 'TRANSICION', 'ARIANNA MARCELA', 'SALCEDO TAPIA', '', '', '2041-06-05', 'F', '3014888623', 'BLEY1110@HOTMAIL.COM', 'CRA 1E Nº 81-17          ', 'O +', 'BLEIDYS JOHANNA TAPIA CANO', '3014888623', 'COOMEVA', '2017-07-19 17:12:01'),
(1048077982, 'R.C.', 'JARDIN', 'MARIA JOSE', 'BUSTAMANTE GUZMAN', '', '', '2041-11-05', 'F', '3016937375', 'DAMA_GUZ28@HOTMAIL.COM', 'CALLE 94 N 4A 37         ', 'A +', 'DALYS MARIA GUZMAN GARCIA', '3016937375', 'CAFE SALUD                    ', '2017-07-19 17:11:59'),
(1048078165, 'NUIP', '2', 'LIKAR CAMILO', 'CAMPO RODRIGUEZ', '', '', '0000-00-00', 'M', '3004254698', 'yanethsusana09@gmail.com', 'CLL 72C Nº 23C-12        ', '', 'YANETH SUSANA RODRIGUEZ MARTINEZ', '3007254698', '', '2017-07-19 17:11:48'),
(1048212522, 'NUIP', '2', 'ROYMAR ARMANDO', 'VARELA COMAS', '', '', '2040-08-09', 'M', '3135977527', '', 'CARRERA 2 N° 62 - 24     ', 'O -', 'JINNA ESTHER COMAS FORTICH', '3135977527', 'SALUD TOTAL', '2017-07-19 17:11:49'),
(1050549967, 'NUIP', '1', 'YISED ', 'FELIZZOLA PEREZ', '', '', '2040-12-03', 'F', '3106216629', 'beatrizpereznavarro1903@gmail.', 'CRA 7 SUR Nº 90-17       ', 'O +', 'BEATRIZ  PEREZ NAVARRO', '3204510878', 'SURA                          ', '2017-07-19 17:11:45'),
(1051826208, 'NUIP', '2', 'HERNANDO JAVIER', 'HERNANDEZ SIERRA', '', '', '0000-00-00', 'M', '3013673554', '', 'CALLE 78 Nº 2 D - 54     ', 'A +', 'ANA MARIA SIERRA ALVAREZ', '3046672394', 'SALUD VIDA                    ', '2017-07-19 17:11:48'),
(1066520740, 'NUIP', '1', 'MIGUEL ÁNGEL', 'VERGARA BETTIN', '', '', '0000-00-00', 'M', '3004608799', '', 'CALLE 94 NO 2D-05        ', 'O +', 'MARIA LETH BETTIN LOPEZ', '3004608799', 'Cafesalud                     ', '2017-07-19 17:11:45'),
(1069646998, 'NUIP', 'TRANSICION', 'LAURA ', 'GARCIA SERNA', '', '', '0000-00-00', 'F', '3022362448', 'jduvan.46@hotmail.com', 'CLL 76 Nº 5F-04          ', 'O +', 'ADRIANA GISEL SERNA ZULUAGA', '3022362448', 'XXX                           ', '2017-07-19 17:12:02'),
(1073976135, 'T.I.', '6', 'DARCY LILIANA', 'DAVILA MARTINEZ', '', '', '0000-00-00', 'F', '3108220520', 'xxxx@hotmail.com', 'CRA 5B Nº 90-26          ', 'O +', 'LEY DIANA MARTINEZ ORTEGA', '3108220520', 'MUTUAL SER                    ', '2017-07-19 17:11:53'),
(1081926848, 'R.C.', 'PREJARDIN', 'CRISTIAN DAVID', 'GARCIA FLOREZ', '', '', '0000-00-00', 'M', '3205842472', '', 'CARRERA 2C # 90 - 73     ', '', 'JENNY LORENA FLOREZ ECHEVERRY', '3205842472', '', '2017-07-19 17:12:00'),
(1082852421, 'NUIP', '7', 'SHAIEL ALEXANDRA', 'BUENO CATALAN', '', '', '0000-00-00', 'F', '3013774620', 'luzmarycatalancarvajal@gmail.c', 'CLL 92 Nº 4-30           ', 'O +', 'LUZ MARY CATALAN CARVAJAL', '3013774620', 'SALUD TOTAL', '2017-07-19 17:11:54'),
(1082902565, 'R.C.', '4', 'NOHELIA JOHANNA', 'BOTTED ARIZA', '', '', '0000-00-00', 'F', '3157113317', 'nlbotted@gmail.com', 'CARRERA 6L # 102 - 27    ', 'A +', 'CARMEN YUDELYS ARIZA GOMEZ', '3008482946', 'COOMEVA', '2017-07-19 17:11:51'),
(1082921753, 'R.C.', '4', 'LUNA VALENTINA', 'BOTTED ARIZA', '', '', '0000-00-00', 'F', '3157113317', 'nlbotted@gmail.com', 'CARRERA 6L 2 # 102 - 27  ', 'A +', 'CARMEN YUDELYS ARIZA GOMEZ', '3008482946', 'COOMEVA', '2017-07-19 17:11:51'),
(1082949191, 'R.C.', '2', 'SARA ALEJANDRA', 'ORTIZ OSORIO', '', '', '0000-00-00', 'F', '3206425200', 'YISY_JUSTIN@HOTMAIL.COM', 'CARRERA 34 # 60 - 45     ', 'A +', 'YISEIDIS DALLANA OSORIO DE LA HOZ', '3226087053', 'SALUDCOOP                     ', '2017-07-19 17:11:49'),
(1096804498, 'R.C.', '3', 'MARIA VALENTINA', 'HERNANDEZ RIPOLL', '', '', '0000-00-00', 'F', '3136845958', '', 'CARRERA 6L1 # 101-22     ', 'O +', 'KAREN YULIANA RIPOLL CARDOZO', '3166224621', 'COOMEVA', '2017-07-19 17:11:49'),
(1096805611, 'NUIP', '2', 'ANDRES FELIPE', 'BUSTAMANTE CASTILLO', '', '', '2040-06-03', 'M', '3114288996', 'vane110380@hotmail.com', 'CRA 3C Nº 91-60          ', 'O +', 'VANESSA  CASTILLO ARQUEZ', '3114288996', 'SALUD COOP                    ', '2017-07-19 17:11:48'),
(1098456019, 'T.I.', '6', 'MARIA FERNANDA', 'ANGARITA PICO', '', '', '0000-00-00', 'F', '3216952220', 'xxx@HOTMAIL.COM', 'CRA 6 N° 90-80 EL ROMANCE', 'B +', 'DEISY JANETH PICO LEAL', '3215885198', 'COLSALUD                      ', '2017-07-19 17:11:53'),
(1101881223, 'NUIP', 'TRANSICION', 'JESUS MIGUEL', 'BLANCO DIAZ', '', '', '0000-00-00', 'M', '3215852534', '', 'CRA 5C NO 99B-79. VILLA N', 'AB +', 'LEONELA  BLANCO DIAZ', '3016890142', 'Salud vida                    ', '2017-07-19 17:12:02'),
(1102634216, 'T.I.', '8', 'YERIKA VIVIANA', 'DE LA HOZ GONZALEZ', '', '', '0000-00-00', 'F', '3209045626', 'mytanis@hotmail.es', 'CLL 90 Nº 4B-03          ', 'A +', 'TANIA PATRICIA GONZALEZ ORDOÑEZ', '3168741916', 'SALUD COOP                    ', '2017-07-19 17:11:57'),
(1123207879, 'T.I.', '4', 'JUAN CAMILO', 'GONZALEZ CEBALLOS', '', '', '0000-00-00', 'M', '3017076193', 'heidyceballos@outlook.es', 'CLL 92 Nº 4-30           ', 'O +', 'ARNULFO JOSE GONZALEZ BUENO', '3017076193', 'SALUD TOTAL', '2017-07-19 17:11:51'),
(1123801562, 'T.I.', '7', 'DAYAN MISHEL', 'ECHEVERRY CAMACHO', '', '', '0000-00-00', 'F', '3007633861', '', 'CONJUNTO 8 TORRE 7 APTO 2', 'O +', 'MADELCY DEL CARMEN PADILLA PADILLA', '', 'MUTUAL SER                    ', '2017-07-19 17:11:55'),
(1127596603, 'R.C.', '3', 'EMELYN PAOLA', 'REDONDO PERTUZ', '', '', '0000-00-00', 'F', '3013491079', '', 'CARRERA 4C # 92 - 19 PISO', 'O +', 'KAREN PATRICIA PERTUZ LEAL', '3002169374', 'SALUD TOTAL', '2017-07-19 17:11:50'),
(1127596604, 'NUIP', '1', 'THAIS CAMILA', 'REDONDO PERTUZ', '', '', '0000-00-00', 'F', '3002169374', '', 'CARRERA 4C #92 - 19 PISO ', 'O +', 'KAREN PATRICIA PERTUZ LEAL', '3002169374', 'SALUD TOTAL', '2017-07-19 17:11:45'),
(1129506037, 'T.I.', '7', 'MARITZABEL ', 'SERNA ROSADO', '', '', '0000-00-00', 'F', '3107126869', '', 'CALLE 94 # 3A - 34       ', 'O +', 'REBECA ISABEL RODRIGUEZ DE ROSADO', '3042086294', 'SALUDCOOP                     ', '2017-07-19 17:11:56'),
(1130272783, 'NUIP', '1', 'SHEILY CAROLINA', 'HERRERA BELTRAN', '', '', '0000-00-00', 'F', '3017539212', '', 'CARRERA 6 J Nº 101 - 57  ', 'A +', 'MARLENYS PAOLA BELTRAN RODRIGUEZ', '3241255', 'CLINICA DE LA POLICIA         ', '2017-07-19 17:11:45'),
(1131284053, 'NUIP', '1', 'GABRIEL ANDRES', 'PICO RODRIGUEZ', '', '', '0000-00-00', 'M', '3017658517', 'xxx@hotmail.com', 'CRA 1 Nº 91-04           ', 'O +', 'GABRIEL  PICO LEAL', '3218279286', 'CAFESALUD                     ', '2017-07-19 17:11:45'),
(1139424312, 'T.I.', '7', 'RICARDO AIMAR', 'NAVARRO CABALLERO', '', '', '0000-00-00', 'M', '3012931975', 'marielacaba01@hotmail.com', 'CRA 5B Nº 91-66          ', 'O +', 'MARIELA  CABALLERO HEREDIA', '3012931975', 'SALUD COOP                    ', '2017-07-19 17:11:55'),
(1139426361, 'R.C.', '5', 'MARINA ANDREA', 'PEREZ GUTIERREZ', '', '', '2039-05-04', 'F', '3206330406', '', 'CALLE 91 # 6B - 66       ', 'O +', 'NELSON FERNEY PEREZ FANDIÑO', '3106917267', 'SALUD COOP                    ', '2017-07-19 17:11:53'),
(1139426510, 'T.I.', '5', 'LAURA VANESSA', 'MEDRANO LOZANO', '', '', '2039-08-06', 'F', '3015944220', '', 'CALLE 90 B # 10 S - 95   ', 'A +', 'GRACIELA  LOZANO ROJAS', '3015640897', 'CAPRECOM                      ', '2017-07-19 17:11:53'),
(1139426600, 'T.I.', '5', 'CAMILO JOSE', 'BUSTAMANTE GUZMAN', '', '', '0000-00-00', 'M', '3016937375', 'lainerguzman09@hotmail.com', 'CLL 94 Nº 4A-37          ', 'O +', 'DALYS MARIA GUZMAN GARCIA', '3016937375', 'SURA                          ', '2017-07-19 17:11:52'),
(1139426986, 'T.I.', '5', 'ISABELLA ', 'BERRIO AMADOR', '', '', '0000-00-00', 'F', '3103678751', 'ASTRIDAMADOR@COLSAM.EDU.CO', 'CRA 3 N° 88-78           ', '', 'ASTRID CECILIA AMADOR OVIEDO', '3103678751', '', '2017-07-19 17:11:52'),
(1139427700, 'NUIP', '4', 'ADRIAN RAMIRO', 'CONEO MARIN', '', '', '0000-00-00', 'M', '3013084022', 'adrianconeo28@hotmail.com', 'CRA 1E Nº 94-31          ', 'A +', 'DIANA MARGARITA MARIN PEREZ', '3013084022', 'NINGUNA                       ', '2017-07-19 17:11:51'),
(1139428575, 'T.I.', '4', 'BREYNER ANDRES', 'ROJANO RUEDA', '', '', '0000-00-00', 'M', '3145739527', 'breyner311@hotmail.com', 'CLL 80 Nº 4B-62          ', 'O -', 'LUDIETH PAOLA RUEDA CARRASCAL', '3015164704', 'COMPARTA                      ', '2017-07-19 17:11:51'),
(1139430992, 'R.C.', '2', 'LUIS ANGEL', 'URIBE ESTOR', '', '', '2040-08-08', 'M', '3107015865', '', 'CARRERA 7 B  99C - 82    ', 'A +', 'YULIANA MARGARITA ESTOR QUESADA', '3107015865', 'SALUD COOP                    ', '2017-07-19 17:11:49'),
(1139431300, 'R.C.', '2', 'GABRIELA ', 'CHINCHIA PERALTA', '', '', '0000-00-00', 'F', '3006932540', '', 'CARRERA 6 L 3 # 101 - 42 ', 'O +', 'XIOMARA  PERALTA HERNANDEZ', '3006932540', 'SALUD COOP                    ', '2017-07-19 17:11:48'),
(1139431493, 'R.C.', '2', 'SAJOHA PAOLA', 'VASQUEZ RODRIGUEZ', '', '', '0000-00-00', 'F', '3116993964', '', 'DIAGONAL 88 # 1F - 32    ', 'A +', 'JOHANA CECILIA RODRIGUEZ MEZA', '3205188760', 'SALUD TOTAL', '2017-07-19 17:11:49'),
(1139432283, 'NUIP', '1', 'SCARLET JULISA', 'PEREZ BOADA', '', '', '0000-00-00', 'F', '3106212024', '', 'CRA 3 Nº 91-61           ', 'O +', 'LEDYS ZULEIMA BOADA ', '3106212024', 'SALUD COOP                    ', '2017-07-19 17:11:45'),
(1139432730, 'NUIP', '1', 'GABRIELA VALENTINA', 'PEREZ GUTIERREZ', '', '', '0000-00-00', 'F', '3206330406', 'yorlanis@hotmail.com', 'CLL 91 N° 6B-66          ', 'B +', 'NELSON FERNEY PEREZ FANDIÑO', '3106917267', 'SALUD COOP                    ', '2017-07-19 17:11:45'),
(1140834068, 'T.I.', '3', 'CAMILO ANDRES', 'ROJANO BARRIOS', '', '', '0000-00-00', 'M', '3043925901', '', 'CALLE 98C N 3C-14 CONJUNT', 'O +', 'JOSE LUIS MENDOZA NARVAEZ', '3043925901', 'CAJA COPI                     ', '2017-07-19 17:11:50'),
(1140835780, 'R.C.', '3', 'JUAN DIEGO', 'LOPEZ BARRIOS', '', '', '0000-00-00', 'M', '3006722457', 'YURIBARRIOS38@GMAIL.COM', 'CALLE 98C N 3C-13 CONJUNT', 'O +', 'YURI PAOLA BARRIOS LONDOÑO', '3006722457', 'SURA                          ', '2017-07-19 17:11:50'),
(1140844103, 'R.C.', '2', 'IBRAYN ALIN', 'MARTINEZ JANNE', '', '', '2040-07-05', 'M', '3006656559', '', 'CALLE 96A # 6 SUR - 11   ', 'O +', 'JUAN CARLOS MARTINEZ DOMINGUEZ', '3006656569', 'NUEVA EPS                     ', '2017-07-19 17:11:48'),
(1143116628, 'T.I.', '4', 'JOYBER DANIEL', 'BELTRAN FONTALVO', '', '', '0000-00-00', 'M', '3015092138', 'elpesao_02@hotmail.com', 'CRA 4 SUR N° 98-09       ', 'O +', 'BEATRIZ ELENA FONTALVO GARCIA', '3015092138', 'SALUD COOP                    ', '2017-07-19 17:11:51'),
(1143129274, 'NUIP', '2', 'HEIDY CAROLINA', 'GONZALEZ CEBALLOS', '', '', '2040-03-07', 'F', '3024307193', '', 'CLL 92 Nº 4-30           ', 'A +', 'ARNULFO JOSE GONZALEZ BUENO', '3017076193', '', '2017-07-19 17:11:48'),
(1143165505, 'C.C.', '10', 'LINA ROSA', 'PEREZ BOLAÑOS', '', '', '0000-00-00', 'F', '3102088323', '', 'CLL 90 Nº 6H-06          ', 'O +', 'DANITH MARIA GUZMAN OSORIO', '3102088323', '', '2017-07-19 17:11:46'),
(1143238356, 'T.I.', '3', 'ELIANA MARCELA', 'MEJIA BOLIVAR', '', '', '0000-00-00', 'F', '3233394778', '', 'CARRERA 6 L2  # 102 - 75 ', 'O +', 'CINDY MARCELA BOLIVAR CERVANTES', '3135635454', 'BARRIOS UNIDOS DE QUIBDO      ', '2017-07-19 17:11:50'),
(1143241184, 'R.C.', '2', 'MATHIAS JOSE', 'HERNANDEZ BERMUDEZ', '', '', '0000-00-00', 'M', '3103607641', '', 'CALLE 94#2C-26           ', 'O +', 'PATRICIA  DONADO VALENCIA', '3014610172', 'Medies, Salud Total           ', '2017-07-19 17:11:48'),
(1143331331, 'T.I.', '5', 'FABIAN JOSÉ', 'QUINTANA OLIVARES', '', '', '0000-00-00', 'M', '3103610893', '', 'CARRERA 3C  92 - 70      ', 'A +', 'YUDIS  OLIVARES MORENO', '3103610893', 'SALUD TOTAL', '2017-07-19 17:11:53'),
(1143353677, 'NUIP', '2', 'YULIETH SOFIA', 'QUINTANA OLIVARES', '', '', '2040-08-03', 'F', '3103610893', 'xxxx@hotmail.com', 'CRA 3C Nº 92-70          ', 'A +', 'YUDIS  OLIVARES MORENO', '3103610893', 'SALUD TOTAL', '2017-07-19 17:11:48'),
(1158463481, 'R.C.', '2', 'DANIELA ', 'HOYOS BARRIOS', '', '', '0000-00-00', 'F', '3014240895', 'lfalquez2008@hotmail.com', 'CRA 3A Nº 90-64          ', 'O +', 'ANA LUCIA BARRIOS BENAVIDES', '3014240895', 'COMPARTA                      ', '2017-07-19 17:11:48'),
(1192767873, 'T.I.', '11', 'BREINER ', 'RODRIGUEZ GIL', '', '', '0000-00-00', 'M', '3226961842', '', 'CLL 91 Nº 4-21           ', 'O +', 'LICETH MARIA GIL PEREZ', '3226961842', 'COOMEVA', '2017-07-19 17:11:47'),
(1192781055, 'T.I.', '11', 'JUAN DAVID', 'PASTRANA BARRIOS', '', '', '0000-00-00', 'M', '3147180920', 'xxx@hotmail.com', 'CLL 96 Nº 2B-20          ', 'O +', 'MIRYAM ARMENIA BARRIOS VILORIA', '3147180920', 'CAJACOPI                      ', '2017-07-19 17:11:47'),
(1192925727, 'T.I.', '11', 'LEONARDO ANDRES', 'ARCOS GARCIA', '', '', '0000-00-00', 'M', '3012755060', '', 'CRA. 9G N° 131-88        ', 'O +', 'MONICA ISABEL GARCIA VILLARREAL', '3012755060', 'COOMEVA', '2017-07-27 17:10:12'),
(1193093133, 'T.I.', '7', 'VERONICA MARIA', 'MORALES HORTA', '', '', '0000-00-00', 'F', '3016539266', 'monicahortan@hotmail.com', 'CALLE 92 Nº 3A 05        ', 'O +', 'MONICA ESTHER HORTA NARANJO', '3145664717', 'COOMEVA', '2017-07-19 17:11:55'),
(1193102348, 'T.I.', '8', 'YUNAISY DE JESUS', 'HERNANDEZ CARDENAS', '', '', '0000-00-00', 'F', '3182632114', 'jairo.hernandez@hotmail.es', 'CRA 3B Nº 92-60          ', '', 'JAIRO RAFAEL HERNANDEZ BOLAÑO', '3182632114', '', '2017-07-19 17:11:57'),
(1193444911, 'T.I.', '11', 'JOSEPH JAIR', 'FLOREZ LOPEZ', '', '', '0000-00-00', 'M', '3007372093', 'michael-263@live.com', 'CRA 4C N° 91-80          ', 'A +', 'ANA ISABEL LOPEZ OLIVERA', '3007372093', 'SALUD TOTAL', '2017-07-19 17:11:47'),
(1193587502, 'T.I.', '8', 'INGRID JIMENA', 'TRUJILLO BENAVIDEZ', '', '', '0000-00-00', 'F', '3217703708', 'shirly1992@hotmail.es', 'CRA 7 SUR N° 90-17       ', 'O +', 'SHIRLY MARIA GONZALEZ VILORIA', '3217703708', 'SURA EPS                      ', '2017-07-19 17:11:56'),
(1193593206, 'T.I.', '8', 'MARIA CLAUDIA', 'CARRASQUILLA ARIZA', '', '', '0000-00-00', 'F', '3012807359', '', 'CARRERA 6L 3 # 102 - 69  ', 'A +', 'CLARITZA  ARIZA HIGGINS', '3012807359', 'MUTUAL SER                    ', '2017-07-19 17:11:56'),
(1194963505, 'R.C.', '3', 'MELANIE AIDES', 'GARCIA MENDOZA', '', '', '0000-00-00', 'F', '3107349039', '', 'CARRERA 3 # 80-37        ', '', 'ZULMIRA DEL CARMEN MENDOZA SALGADO', '3126509348', 'SANITAS                       ', '2017-07-19 17:11:49'),
(1194968084, 'R.C.', 'TRANSICION', 'MARIA CAMILA', 'BOSSIO SUAREZ', '', '', '0000-00-00', 'F', '3106678836', 'BOSSIO72@HOTMAIL.COM', 'CCL 98C Nº 3C-14         ', '', 'ROSA MARIA SUAREZ MEJIA', '', '', '2017-07-19 17:12:01'),
(1194968793, 'NUIP', 'TRANSICION', 'THALIANA ', 'TORRES ALVAREZ', '', '', '2041-10-01', 'F', '3107280334', 'XXX@HOTMAIL.COM', 'CLL 92 Nº 4B-06          ', 'A +', 'RODRIGO DE JESÚS TORRES TORRES', '3107280334', 'SALUD TOTAL', '2017-07-19 17:12:01'),
(1194970390, 'R.C.', 'TRANSICION', 'JOSE BAU', 'MESA DE LOS REYES', '', '', '0000-00-00', 'M', '3016149011', '', 'CARRERA 4 # 94-56        ', 'A +', 'JADIBYS  DE LOS REYES RODRIGUEZ', '3216662903', 'SALUD TOTAL', '2017-07-19 17:12:01'),
(1194970827, 'NUIP', 'PREJARDIN', 'THIAGO ', 'COLMENARES PARRA', '', '', '0000-00-00', 'M', '3103626224', '', 'CALLE 90 N 2C -43        ', 'A +', 'GIANINA PAOLA PARRA DE LA HOZ', '3103626224', 'COOSALUD                      ', '2017-07-19 17:12:00'),
(1194971597, 'NUIP', 'PREJARDIN', 'ISABELLA ', 'MEJIA DIAZ', '', '', '0000-00-00', 'F', '3007300819', 'ALEJANDO-2010@HOTMAIL.COM', 'CALLE 94 12-25           ', 'O +', 'YURIS PAOLA DIAZ ROMERO', '3145681636', 'EPS POLICIA NACIONAL          ', '2017-07-19 17:12:00'),
(1201216420, 'NUIP', '2', 'SAMUEL DAVID', 'PARDO SALTARIN', '', '', '0000-00-00', 'M', '3107092043', 'lpcepeda81@yahoo.es', 'CRA 27 N° 47-47          ', 'A +', 'LEONARDO JOSE PARDO CEPEDA', '3107092043', 'SURA                          ', '2017-07-19 17:11:49'),
(2147483647, 'T.I.', '11', 'JUAN DAVID', 'GARCIA ', '', '', '0000-00-00', 'M', '3013254458', 'miquecreativo@outlook.com', 'CRA 6E Nº 91-58          ', 'O +', 'MIGUEL RAMIRO VELASQUEZ RIOS', '3013254458', 'NUEVA EPS                     ', '2017-07-19 17:11:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `dni` int(15) NOT NULL,
  `TDNI` varchar(20) DEFAULT NULL,
  `NAMES` varchar(50) DEFAULT NULL,
  `FIRTSNAME` varchar(50) DEFAULT NULL,
  `PHONE` varchar(10) DEFAULT NULL,
  `EMAIL` varchar(30) DEFAULT NULL,
  `USER` varchar(15) DEFAULT NULL,
  `PASSWORD` varchar(20) DEFAULT NULL,
  `TS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`dni`, `TDNI`, `NAMES`, `FIRTSNAME`, `PHONE`, `EMAIL`, `USER`, `PASSWORD`, `TS`) VALUES
(123456789, 'CC', 'Estefanía', 'Apellidos', '3122121512', 'asdasdasdasdsa', 'admin', '12345', '2017-06-15 18:03:56');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aquestion`
--
ALTER TABLE `aquestion`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `clinichistory`
--
ALTER TABLE `clinichistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clinichistory_ibfk_1` (`dni_student`),
  ADD KEY `clinichistory_ibfk_2` (`id_aquestion`),
  ADD KEY `clinichistory_ibfk_3` (`id_diseases`);

--
-- Indices de la tabla `diseases`
--
ALTER TABLE `diseases`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`dni`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aquestion`
--
ALTER TABLE `aquestion`
  MODIFY `ID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `clinichistory`
--
ALTER TABLE `clinichistory`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `diseases`
--
ALTER TABLE `diseases`
  MODIFY `ID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clinichistory`
--
ALTER TABLE `clinichistory`
  ADD CONSTRAINT `clinichistory_ibfk_1` FOREIGN KEY (`dni_student`) REFERENCES `students` (`dni`),
  ADD CONSTRAINT `clinichistory_ibfk_2` FOREIGN KEY (`id_aquestion`) REFERENCES `aquestion` (`ID`),
  ADD CONSTRAINT `clinichistory_ibfk_3` FOREIGN KEY (`id_diseases`) REFERENCES `diseases` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
